package com.dataart.practice.cm.service;

import com.dataart.practice.cm.data.dao.UserDao;
import com.dataart.practice.cm.data.exceptions.DatabaseDriverAbsentException;
import com.dataart.practice.cm.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by aovsyannikov on 8/1/2016.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserService {

    @Autowired
    private UserDao userDao;

    public boolean isLoginTaken(String login) throws DatabaseDriverAbsentException, SQLException, IOException {
        return userDao.isLoginTaken(login);
    }

    public User getUserByLogin(String login) throws Exception {
        return userDao.getUserByLogin(login);
    }

    public User getUserById(long userID) throws Exception {
        return userDao.getUserByID(userID);
    }

    public void registerUser(User user) throws DatabaseDriverAbsentException, SQLException, IOException {
        userDao.registerUser(user);
    }

    public void deleteUser(long userID) throws Exception {
        userDao.deleteUser(userID);
    }

    public List<User> getAllUsers() throws DatabaseDriverAbsentException, SQLException, IOException {
        return userDao.getAllUsers();
    }

    public void setNewLogin(long userID, String newLogin) throws DatabaseDriverAbsentException, SQLException,
            IOException {
        userDao.setNewLogin(userID, newLogin);
    }

    public void setNewUsername(long userID, String newUsername) throws DatabaseDriverAbsentException,
            SQLException, IOException {
        userDao.setNewUsername(userID, newUsername);
    }

    public void setNewPassword(long userID, String newPassword) throws NoSuchAlgorithmException,
            DatabaseDriverAbsentException, SQLException, IOException {
        userDao.setNewPassword(userID, newPassword);
    }

    public void setNewDescription(long userID, String newDescription) throws DatabaseDriverAbsentException,
            SQLException, IOException {
        userDao.setNewDescription(userID, newDescription);
    }
}



