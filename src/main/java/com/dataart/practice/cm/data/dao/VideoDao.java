package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.Video;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public interface VideoDao extends GenericDao<Video, Long> {
}
