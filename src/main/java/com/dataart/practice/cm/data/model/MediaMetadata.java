package com.dataart.practice.cm.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Entity(name = "MediaMetadata")
@Table(name = "media_metadata")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MediaMetadata implements Serializable {

    @Id
    @Column(name = "id")
    private long mediaId;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @Column(name = "name")
    private String fileName;

    @Column(name = "drive_id")
    private String driveID;

    @Column(name = "classtype")
    private String type;

    private long date;

    private int size;

    private String extension;

    public MediaMetadata() {
    }

    public static MetadataBuilder getMetadataBuilder() {
        return new MetadataBuilder();
    }

    public long getMediaId() {
        return mediaId;
    }

    public void setMediaId(long mediaId) {
        this.mediaId = mediaId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDriveID() {
        return driveID;
    }

    public void setDriveID(String driveID) {
        this.driveID = driveID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public static class MetadataBuilder {

        private MediaMetadata metadata;

        public MetadataBuilder() {
            metadata = new MediaMetadata();
        }

        public MetadataBuilder mediaId(long id) {
            metadata.setMediaId(id);
            return this;
        }

        public MetadataBuilder driveId(String driveId) {
            metadata.setDriveID(driveId);
            return this;
        }

        public MetadataBuilder user(User user) {
            metadata.setUser(user);
            return this;
        }

        public MetadataBuilder type(String type) {
            metadata.setType(type);
            return this;
        }

        public MetadataBuilder filename(String name) {
            metadata.setFileName(name);
            return this;
        }

        public MetadataBuilder date(long date) {
            metadata.setMediaId(date);
            return this;
        }

        public MetadataBuilder size(int size) {
            metadata.setSize(size);
            return this;
        }

        public MetadataBuilder extension(String ext) {
            metadata.setExtension(ext);
            return this;
        }

        public MediaMetadata build() {
            return this.metadata;
        }
    }
}
