package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.exceptions.DatabaseDriverAbsentException;
import com.dataart.practice.cm.data.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 8/17/2016.
 */
public abstract class UserDaoImpl implements UserDao {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void registerUser(User user) throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Override
    public boolean isLoginTaken(String login) throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        return session.byNaturalId(User.class).using("login", login).loadOptional().isPresent();
    }

    @Override
    public User getUserByLogin(String login) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        User user = session.byNaturalId(User.class).using("login", login).load();
        return user;
    }

    @Override
    public User getUserByID(long userID) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setPhotosList(
                user
                        .getPhotosList()
                        .stream()
                        .sorted()
                        .collect(Collectors.toList())
        );
        user.setVideosList(
                user
                        .getVideosList()
                        .stream()
                        .sorted()
                        .collect(Collectors.toList())
        );
        return user;
    }

    @Override
    public void deleteUser(long userID) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        session
                .getNamedQuery("deleteUserPhotos")
                .setParameter("userID", userID)
                .executeUpdate();
        session
                .getNamedQuery("deleteUserVideos")
                .setParameter("userID", userID)
                .executeUpdate();
        session
                .getNamedQuery("deleteUser")
                .setParameter("userID", userID)
                .executeUpdate();
    }

    @Override
    public List<User> getAllUsers() throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        return session
                .getNamedQuery("getAllUsers")
                .list();
    }

    @Override
    public void setNewLogin(long userID, String newLogin) throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setLogin(newLogin);
        session.saveOrUpdate(user);
    }

    @Override
    public void setNewUsername(long userID, String newUsername) throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setUserName(newUsername);
        session.saveOrUpdate(user);
    }

    @Override
    public void setNewPassword(long userID, String newPassword) throws NoSuchAlgorithmException,
            DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setPassword(newPassword);
        session.saveOrUpdate(user);
    }

    @Override
    public void setNewDescription(long userID, String newDescription) throws DatabaseDriverAbsentException,
            SQLException {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setDescription(newDescription);
        session.saveOrUpdate(user);
    }
}
