package com.dataart.practice.cm.rest.resource.impl;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.data.model.Video;
import com.dataart.practice.cm.rest.resource.BaseResource;
import com.dataart.practice.cm.rest.resource.UserMediaResource;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserMediaResourceImpl extends BaseResource implements UserMediaResource {

    private static final int MAX_PHOTO_FILE_SIZE = 52428800; //50MB
    private static final int MAX_VIDEO_FILE_SIZE = 1073741824; //1GB

    @Cacheable("userPhotos")
    public ResponseEntity<List<Resource>> getUserPhotos(
            @PathVariable(value = "userID")
                    long userID,
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                    int pageSize) {
        LinkedList<Resource> resourceList = new LinkedList<>();
        try {
            for (Photo photo : mediaService.getUserPhotos(userID)) {
                resourceList.addLast(turnPhotoToResource(photo));
            }
            return performPagination(resourceList, page, pageSize);
        } catch (Exception e) {
            resourceList.add(new Resource<>(e.getMessage()));
        }
        return new ResponseEntity<>(resourceList, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @CacheEvict(cacheNames = {"photos", "userPhotos"}, key = "#userID", allEntries = true)
    public ResponseEntity<Resource> uploadPhoto(@PathVariable(value = "userID") long userID,
                                                @RequestParam(value = "file") MultipartFile file,
                                                HttpSession session) {

        boolean isPhotoSizeNormal = file.getSize() <= (MAX_PHOTO_FILE_SIZE);
        boolean hasName = !StringUtils.isEmpty(file.getName());
        String responseMessage = "Photo upload failed due to exception: ";
        HttpStatus status = HttpStatus.CREATED;
        Resource<String> resource = new Resource<>(responseMessage);
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                boolean isImage = file
                        .getContentType()
                        .contains("image")
                        &&
                        tikaDetector
                                .detect(file.getBytes())
                                .contains("image");
                if (isImage && isPhotoSizeNormal && hasName) {
                    Photo photo = new Photo(file.getBytes(),
                            currentUser,
                            file.getOriginalFilename(),
                            System.currentTimeMillis(),
                            (int) file.getSize(),
                            file.getContentType().substring(6)
                    );
                    mediaService.uploadPhoto(photo);
                    currentUser
                            .getPhotosList()
                            .add(0, photo);
                    responseMessage = "Photo was successfully uploaded";
                    resource = new Resource<>(responseMessage);
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("photos")
                                    .slash(photo.getPhotoID())
                                    .withSelfRel());
                } else if (!isImage) {
                    responseMessage = "This is not an image file";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else if (!isPhotoSizeNormal) {
                    responseMessage = "This image file exceeds maximum allowed size of 50MB";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else if (!hasName) {
                    responseMessage = "This image file does not have name";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                resource = new Resource<>(responseMessage);
            }
        } else {
            responseMessage = "Only user himself can add photos to his collection";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    public ResponseEntity<Resource> getUserPhotosCount(@PathVariable(value = "userID") long userID) {
        Resource resource;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.countUserPhotos(userID));
            resource
                    .add(linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("photos")
                            .withRel("userPhotos"));
        } catch (Exception e) {
            resource = new Resource<>(e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @Cacheable("userPhotos")
    public ResponseEntity<Resource> getPhoto(@PathVariable(value = "photoID") long photoID,
                                             @PathVariable(value = "userID") long userID) {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.getPhotoById(photoID));
            resource
                    .add(linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("photos")
                            .slash(photoID)
                            .withSelfRel());
            resource
                    .add(linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .withRel("user"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"photos", "userPhotos"}, allEntries = true)
    public ResponseEntity<Resource> deleteUserPhotoById(@PathVariable(value = "photoID") long photoID,
                                                        HttpSession session) {
        return deletePhoto(photoID, session);
    }

    @Cacheable(value = "userPhotos", key = "#photoID")
    public void downloadUserPhotoById(@PathVariable("photoID") long photoID, HttpServletResponse resp) {
        downloadPhoto(photoID, resp);
    }

    @Cacheable("userVideos")
    public ResponseEntity<List<Resource>> getUserVideos(
            @PathVariable(value = "userID")
                    long userID,
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                    int pageSize) {
        LinkedList<Resource> resourceList = new LinkedList<>();
        try {
            for (Video video : mediaService.getUserVideos(userID)) {
                resourceList.addLast(turnVideoToResource(video));
            }
            return performPagination(resourceList, page, pageSize);
        } catch (Exception e) {
            resourceList.add(new Resource<>(e.getMessage()));
        }
        return new ResponseEntity<>(resourceList, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @CacheEvict(cacheNames = {"videos", "userVideos"}, allEntries = true)
    public ResponseEntity<Resource> uploadVideo(@PathVariable(value = "userID") long userID,
                                                @RequestParam(value = "file") MultipartFile file,
                                                HttpSession session) {

        boolean isVideoSizeNormal = file.getSize() <= (MAX_VIDEO_FILE_SIZE);
        boolean hasName = !StringUtils.isEmpty(file.getName());
        String responseMessage = "Video upload failed due to exception: ";
        HttpStatus status = HttpStatus.CREATED;
        Resource<String> resource = new Resource<>(responseMessage);
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                boolean isMp4Video = file
                        .getContentType()
                        .contains("video/mp4")
                        &&
                        tikaDetector
                                .detect(file.getBytes())
                                .contains("video/mp4");
                if (isMp4Video && isVideoSizeNormal && hasName) {
                    Video video = new Video(file.getBytes(),
                            currentUser,
                            file.getOriginalFilename(),
                            System.currentTimeMillis(),
                            (int) file.getSize(),
                            file.getContentType().substring(6)
                    );
                    mediaService.uploadVideo(video);
                    currentUser
                            .getVideosList()
                            .add(0, video);
                    responseMessage = "Video was successfully uploaded";
                    resource = new Resource<>(responseMessage);
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("videos")
                                    .slash(video.getVideoID())
                                    .withSelfRel());
                } else if (!isMp4Video) {
                    responseMessage = "This is not a video file";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else if (!isVideoSizeNormal) {
                    responseMessage = "This video file exceeds maximum allowed size of 1GB";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else if (!hasName) {
                    responseMessage = "This video file does not have name";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                status = HttpStatus.INTERNAL_SERVER_ERROR;
                resource = new Resource<>(responseMessage);
            }
        } else {
            responseMessage = "Only user himself can add videos to his collection";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    public ResponseEntity<Resource> getUserVideosCount(@PathVariable(value = "userID") long userID) {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.countUserVideos(userID));
            resource
                    .add(linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("videos")
                            .withRel("userVideos"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @Cacheable("userVideo")
    public ResponseEntity<Resource> getUserVideo(@PathVariable(value = "videoID") long videoID,
                                                 @PathVariable(value = "userID") long userID) {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.getVideoById(videoID));
            resource.add(linkTo(UsersResourceImpl.class).slash(userID).slash("videos").slash(videoID).withSelfRel());
            resource.add(linkTo(UsersResourceImpl.class).slash(userID).withRel("user"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"videos", "userVideos"}, allEntries = true)
    public ResponseEntity<Resource> deleteUserVideoById(@PathVariable(value = "videoID") long videoID,
                                                        HttpSession session) {
        return deleteVideo(videoID, session);
    }

    @Cacheable(value = "userVideos", key = "#videoID")
    public void downloadUserVideoById(@PathVariable("videoID") long videoID, HttpServletResponse resp) {
        downloadVideo(videoID, resp);
    }
}
