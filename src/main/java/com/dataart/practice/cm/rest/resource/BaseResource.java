package com.dataart.practice.cm.rest.resource;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.data.model.Video;
import com.dataart.practice.cm.rest.resource.impl.PhotosResourceImpl;
import com.dataart.practice.cm.rest.resource.impl.UsersResourceImpl;
import com.dataart.practice.cm.rest.resource.impl.VideosResourceImpl;
import com.dataart.practice.cm.service.MediaService;
import com.dataart.practice.cm.service.UserService;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/9/2016.
 */
public abstract class BaseResource {

    @Autowired
    protected UserService userService;

    @Autowired
    protected MediaService mediaService;

    protected Tika tikaDetector = new Tika();

    @Value("${cache}")
    protected String cacheControl;

    protected void downloadPhoto(long photoID, HttpServletResponse resp) {
        try {
            Photo photo = mediaService.getPhotoById(photoID);
            byte[] imageArray = photo.getImageBytes();
            String fileName = photo.getFileName();
            String contentType = "image/" + photo.getExtension();
            resp.setContentType(contentType);
            resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream out = resp.getOutputStream();
            out.write(imageArray);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void downloadVideo(long videoID, HttpServletResponse resp) {
        try {
            Video video = mediaService.getVideoById(videoID);
            byte[] videoArray = video.getVideoBytes();
            String fileName = video.getFileName();
            String contentType = "video/" + video.getExtension();
            resp.setContentType(contentType);
            resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream out = resp.getOutputStream();
            out.write(videoArray);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected ResponseEntity<Resource> deletePhoto(long photoID, HttpSession session) {

        String responseMessage = "Photo removal failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        User currentUser = (User) session.getAttribute("user");
        try {
            currentUser = userService.getUserById(currentUser.getUserID());
            boolean isOwner = currentUser
                    .getPhotosList()
                    .stream()
                    .filter(p -> p.getPhotoID() == photoID)
                    .findAny()
                    .orElse(null) != null;
            if (isOwner || "ADMIN".equals(currentUser.getRole().getRoleName())) {
                mediaService.deletePhoto(photoID);
                if (isOwner) {
                    currentUser.setPhotosList(
                            currentUser
                                    .getPhotosList()
                                    .stream()
                                    .filter(photo -> photo.getPhotoID() != photoID)
                                    .collect(Collectors.toList())
                    );
                }
                responseMessage = "Photo was successfully deleted";
                status = HttpStatus.OK;

            } else {
                responseMessage = "Only owner or admin can delete photos";
                status = HttpStatus.FORBIDDEN;
            }
        } catch (Exception e) {
            responseMessage += e.getMessage();
        }
        return new ResponseEntity<>(new Resource<>(responseMessage), status);
    }

    protected ResponseEntity<Resource> deleteVideo(long videoID, HttpSession session) {
        String responseMessage = "Video removal failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        User currentUser = (User) session.getAttribute("user");
        try {
            currentUser = userService.getUserById(currentUser.getUserID());
            boolean isOwner = currentUser
                    .getVideosList()
                    .stream()
                    .filter(v -> v.getVideoID() == videoID)
                    .findAny()
                    .orElse(null) != null;
            if (isOwner || "ADMIN".equals(currentUser.getRole().getRoleName())) {
                mediaService.deleteVideo(videoID);
                if (isOwner) {
                    currentUser.setVideosList(
                            currentUser
                                    .getVideosList()
                                    .stream()
                                    .filter(video -> video.getVideoID() != videoID)
                                    .collect(Collectors.toList())
                    );
                }
                responseMessage = "Video was successfully deleted";
                status = HttpStatus.OK;

            } else {
                responseMessage = "Only owner or admin can delete videos";
                status = HttpStatus.FORBIDDEN;
            }
        } catch (Exception e) {
            responseMessage += e.getMessage();
        }
        return new ResponseEntity<>(new Resource<>(responseMessage), status);
    }

    protected ResponseEntity<List<Resource>> performPagination(List<Resource> resourceList, int page, int pageSize) {
        PagedListHolder<Resource> listHolder = new PagedListHolder<>(resourceList);
        List<Resource> resultList;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        if ((page == 0 && pageSize == 0) || resourceList.isEmpty()) {
            resultList = resourceList;
        } else if (!resourceList.isEmpty()
                && pageSize != 0
                && page <= (Math.ceil((double) resourceList.size() / (double) pageSize))) {
            listHolder.setPageSize(pageSize);
            listHolder.setPage(page - 1);
            resultList = listHolder.getPageList();
        } else {
            resultList = new ArrayList<>();
            resultList.add(new Resource<>("Invalid pagination parameters"));
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(resultList, headers, status);
    }

    protected Resource<Photo> turnPhotoToResource(Photo photo) {
        Resource<Photo> resource = new Resource<>(photo);
        resource
                .add(linkTo(PhotosResourceImpl.class)
                        .slash(photo.getPhotoID())
                        .withSelfRel());
        resource
                .add(linkTo(PhotosResourceImpl.class)
                        .slash(photo.getPhotoID())
                        .slash("download")
                        .withRel("download"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(photo.getUser().getUserID())
                        .withRel("user"));
        return resource;
    }

    protected Resource<Video> turnVideoToResource(Video video) {
        Resource<Video> resource = new Resource<>(video);
        resource
                .add(linkTo(VideosResourceImpl.class)
                        .slash(video.getVideoID())
                        .withSelfRel());
        resource
                .add(linkTo(VideosResourceImpl.class)
                        .slash(video.getVideoID())
                        .slash("download")
                        .withRel("download"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(video.getUser().getUserID())
                        .withRel("user"));
        return resource;
    }

    protected Resource<User> turnUserToResource(User user) {
        Resource<User> resource = new Resource<>(user);
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .withSelfRel());
        resource
                .add(linkTo(PhotosResourceImpl.class)
                        .withRel("photos"));
        resource
                .add(linkTo(VideosResourceImpl.class)
                        .withRel("videos"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .withRel("users"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("photos")
                        .withRel("userPhotos"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("videos")
                        .withRel("userVideos"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("login")
                        .withRel("login"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("name")
                        .withRel("name"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("password")
                        .withRel("password"));
        resource
                .add(linkTo(UsersResourceImpl.class)
                        .slash(user.getUserID())
                        .slash("description")
                        .withRel("description"));
        return resource;
    }
}
