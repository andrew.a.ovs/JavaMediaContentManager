package com.dataart.practice.cm.data.dao;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import com.dataart.practice.cm.data.model.Video;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("AWSS3")
public class VideoDaoAwsS3Impl extends GenericDaoAwsS3Impl<Video, Long> implements VideoDao {

    @Override
    public Video getById(Long videoID) throws Exception {
        S3Object s3Object = amazonS3.getObject(bucketName, String.valueOf(videoID));
        ObjectMetadata metadata = s3Object.getObjectMetadata();
        return new Video(videoID,
                IOUtils.toByteArray(s3Object.getObjectContent()),
                userDao.getUserByID(Long.parseLong(metadata.getUserMetaDataOf("userID"))),
                metadata.getUserMetaDataOf("filename"),
                Long.parseLong(metadata.getUserMetaDataOf("date")),
                (int) metadata.getContentLength(),
                metadata.getUserMetaDataOf("extension"));
    }

    @Override
    public void upload(Video video) {
        InputStream videoStream = new ByteArrayInputStream(video.getVideoBytes());
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.addUserMetadata("userID", String.valueOf(video.getUser().getUserID()));
        objectMetadata.addUserMetadata("filename", video.getFileName());
        objectMetadata.addUserMetadata("date", String.valueOf(video.getDate()));
        objectMetadata.addUserMetadata("extension", video.getExtension());
        objectMetadata.setContentType("video/" + video.getExtension());
        String key = generateVideoId(video);
        video.setVideoID(Long.parseLong(key));
        amazonS3.putObject(bucketName, key, videoStream, objectMetadata);
    }

    private String generateVideoId(Video video) {
        long sum = video.getDate() + video.getUser().getUserID();
        long diff = video.getDate() - video.getUser().getUserID();
        long uniqueID = sum + diff + video.getSize() % ID_GENERATION_1
                - System.currentTimeMillis() % ID_GENERATION_2;
        return video.getUser().getUserID() + String.valueOf(uniqueID).substring(7);
    }

    @Override
    public long countAll() throws Exception {
        return getAll().size();
    }

    @Override
    public List<Video> getAll() throws Exception {
        List<Video> videosList = new ArrayList<>();
        for (S3ObjectSummary sum : amazonS3.listObjects(bucketName).getObjectSummaries()) {
            S3Object obj = amazonS3.getObject(bucketName, sum.getKey());
            if (obj
                    .getObjectMetadata()
                    .getContentType()
                    .contains("video")) {
                videosList.add(getById(Long.parseLong(sum.getKey())));
                obj.close();
            }
        }
        return videosList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public long countUserMedia(Long userId) throws Exception {
        return getUserMedia(userId).size();
    }

    @Override
    public List<Video> getUserMedia(Long userId) throws Exception {
        List<Video> videosList = new ArrayList<>();
        for (S3ObjectSummary sum : amazonS3.listObjects(bucketName).getObjectSummaries()) {
            S3Object obj = amazonS3.getObject(bucketName, sum.getKey());
            if (obj
                    .getObjectMetadata()
                    .getContentType()
                    .contains("video")
                    && (Long.parseLong(obj.getObjectMetadata().getUserMetaDataOf("userID")) == userId)) {
                videosList.add(getById(Long.parseLong(sum.getKey())));
                obj.close();
            }
        }
        return videosList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
