package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.MediaMetadata;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public abstract class GenericDaoDriveImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

    protected Class<T> entityClass;

    @Autowired
    protected UserDao userDao;

    @Autowired
    protected SessionFactory sessionFactory;

    protected Drive driveService;

    protected final int ID_GENERATION_1 = 10;

    protected final int ID_GENERATION_2 = 1000000000;

    @Autowired
    public GenericDaoDriveImpl(HttpTransport httpTransport, JsonFactory jsonFactory) {
        try {
            GoogleCredential credential = GoogleCredential
                    .fromStream(this
                            .getClass()
                            .getClassLoader()
                            .getResourceAsStream("javamcmgooglecredentials.json"))
                    .createScoped(Collections.singleton(DriveScopes.DRIVE));
            driveService = new Drive.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("JavaMediaContentManager")
                    .build();
            ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                    .getGenericSuperclass();
            this.entityClass = (Class<T>) genericSuperclass
                    .getActualTypeArguments()[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(ID mediaId) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        driveService
                .files()
                .delete(session.load(MediaMetadata.class, mediaId)
                        .getDriveID())
                .execute();
        session.delete(session.load(MediaMetadata.class, mediaId));
    }

    @Override
    public long countAll() throws Exception {
        return (Long) sessionFactory
                .getCurrentSession()
                .getNamedQuery("countAll")
                .setParameter("classtype", entityClass.toString())
                .uniqueResult();
    }

    @Override
    public long countUserMedia(ID userId) throws Exception {
        return (Long) sessionFactory
                .getCurrentSession()
                .getNamedQuery("countUserMedia")
                .setParameter("classtype", entityClass.toString())
                .setParameter("userID", userId)
                .uniqueResult();
    }
}
