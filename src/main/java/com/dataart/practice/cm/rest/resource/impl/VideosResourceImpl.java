package com.dataart.practice.cm.rest.resource.impl;

import com.dataart.practice.cm.data.model.Video;
import com.dataart.practice.cm.rest.resource.BaseResource;
import com.dataart.practice.cm.rest.resource.VideosResource;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class VideosResourceImpl extends BaseResource implements VideosResource {

    @Cacheable("videos")
    public ResponseEntity<List<Resource>> getVideos(
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                    int pageSize) {
        LinkedList<Resource> resourceList = new LinkedList<>();
        try {
            for (Video video : mediaService.getAllVideos().stream().sorted().collect(Collectors.toList())) {
                resourceList.addLast(turnVideoToResource(video));
            }
            return performPagination(resourceList, page, pageSize);
        } catch (Exception e) {
            resourceList.add(new Resource<>(e.getMessage()));
        }
        return new ResponseEntity<>(resourceList, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Resource> getVideosCount() {
        Resource resource = new Resource<>(-1);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.countAllVideos(),
                    linkTo(VideosResourceImpl.class).withRel("videos"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @Cacheable("videos")
    public ResponseEntity<Resource> getVideoById(@PathVariable(value = "videoID") long videoID) {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            Video video = mediaService.getVideoById(videoID);
            resource = turnVideoToResource(video);
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"videos", "userVideos"}, allEntries = true)
    public ResponseEntity<Resource> deleteVideoById(@PathVariable(value = "videoID") long videoID,
                                                    HttpSession session) {
        return deleteVideo(videoID, session);
    }

    @Cacheable(value = "videos", key = "#videoID")
    public void downloadVideoById(@PathVariable("videoID") long videoID, HttpServletResponse resp) {
        downloadVideo(videoID, resp);
    }
}
