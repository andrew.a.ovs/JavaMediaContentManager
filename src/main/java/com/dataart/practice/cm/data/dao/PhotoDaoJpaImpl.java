package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.Photo;
import org.hibernate.Session;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("RDB/Hibernate")
public class PhotoDaoJpaImpl extends GenericDaoJpaImpl<Photo, Long> implements PhotoDao {

    @Override
    public List<Photo> getUserMedia(Long userID) {
        Session session = sessionFactory.getCurrentSession();
        return session
                .getNamedQuery("getUserPhotos")
                .setParameter("userID", userID)
                .list();
    }

    @Override
    public long countUserMedia(Long userID) {
        Session session = sessionFactory.getCurrentSession();
        return (Long) session
                .getNamedQuery("countUserPhotos")
                .setParameter("userID", userID)
                .uniqueResult();
    }
}
