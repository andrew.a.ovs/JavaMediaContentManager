    INSERT INTO roles (id, role_name) VALUES (0, 'USER');

    INSERT INTO roles (id, role_name) VALUES (1, 'ADMIN');

    INSERT INTO users (id, name, login, password, role_id, description, enabled) VALUES ('1', 'John Doe', 'admin', '3d3784f408debc3f6f42fc9358ca3068a3571e332648b9933ab25aa9bcec359ef42a66f6c9e245e2', 1, 'default administrator account', 1);

    INSERT INTO users (id, name, login, password, role_id, description, enabled) VALUES ('2', 'testName1', 'testLogin1', 'c0145bae4e401c784bb3e52f7810ea8e925bbbbb7dd248aedce2c8d51415b17469031edd4fd87e2b', 0, 'testDescr1', 1);

    INSERT INTO users (id, name, login, password, role_id, description, enabled) VALUES ('3','testName2', 'testLogin2', 'f56f3f7d0f1277240b68ccad73dc877635ce7dc55f97181c10576c8b8885dc71369b37ae4a1c4f38', 0, 'testDescr3', 1);

    INSERT INTO users (id, name, login, password, role_id, description, enabled) VALUES ('4', 'testName3', 'testLogin3', '96be64f8f070354ded21589671f9ae41565acd3ab10363cb208467c4e45abe666b0defb816e85d0a', 0, 'testDescr4', 1);

    INSERT INTO users (id, name, login, password, role_id, description, enabled) VALUES ('5', 'testName4', 'testLogin4', 'ea95259073a6fa4b9e8a231f5e2052693e569727b9849855715444fa702a4f18fa3e7d1120734475', 0, 'testDescr5', 1);

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('1', '1473866365650',	'jpeg',	'testPhoto1',	'12345', '1');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('2', '1473866365650',	'jpeg',	'testPhoto2',	'12345', '2');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('3', '1473866365650',	'jpeg',	'testPhoto2',	'12345', '2');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('4', '1473866365650',	'jpeg',	'testPhoto2',	'12345', '2');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('5', '1473866365650',	'jpeg',	'testPhoto3',	'12345', '3');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('6', '1473866365650',	'jpeg',	'testPhoto3',	'12345', '3');

    INSERT INTO photos (id, date, extension, name, size, user_id) VALUES ('7', '1473866365650',	'jpeg',	'testPhoto3',	'12345', '3');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('1', '1473866365650',	'mp4',	'testVideo1',	'1234', '1');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('2', '1473866365650',	'mp4',	'testVideo2',	'1234', '2');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('3', '1473866365650',	'mp4',	'testVideo2',	'1234', '2');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('4', '1473866365650',	'mp4',	'testVideo2',	'1234', '2');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('5', '1473866365650',	'mp4',	'testVideo3',	'1234', '3');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('6', '1473866365650',	'mp4',	'testVideo3',	'1234', '3');

    INSERT INTO videos (id, date, extension, name, size, user_id) VALUES ('7', '1473866365650',	'mp4',	'testVideo3',	'1234', '3');
