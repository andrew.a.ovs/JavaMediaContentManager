package com.dataart.practice.cm.security;

import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aovsyannikov on 8/4/2016.
 */
@Configuration
@PropertySource("classpath:hostname.properties")
public class SecurityConfig {

    @Value("${hostname}")
    private String hostname;

    @Autowired
    private UserService userService;

    @Autowired
    private MappingJackson2HttpMessageConverter messageConverter;

    @Bean
    public AuthenticationEntryPoint authEntryPoint() {
        return new AuthenticationEntryPoint() {
            @Override
            public void commence(HttpServletRequest httpServletRequest,
                                 HttpServletResponse httpServletResponse,
                                 AuthenticationException e) throws IOException, ServletException {
                httpServletResponse.addHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
                httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
                httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
            }
        };
    }

    @Bean
    public AuthenticationSuccessHandler loginSuccessHandler() {
        return new AuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse,
                                                Authentication authentication) throws IOException, ServletException {
                try {
                    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
                    httpServletResponse.setContentType("application/json");
                    UserDetails details = (UserDetails) authentication.getPrincipal();
                    String login = details.getUsername();
                    User user = userService.getUserByLogin(login);
                    httpServletRequest.getSession().setAttribute("user", user);
                    List<Link> links = new ArrayList<>();
                    String selfHref = hostname + "/mcm/api/v1/users/" + user.getUserID();
                    links.add(new Link(selfHref));
                    links.add(new Link(hostname + "/mcm/api/v1/photos").withRel("photos"));
                    links.add(new Link(hostname + "/mcm/api/v1/videos").withRel("videos"));
                    links.add(new Link(hostname + "/mcm/api/v1/users").withRel("users"));
                    links.add(new Link(selfHref + "/photos").withRel("userPhotos"));
                    links.add(new Link(selfHref + "/videos").withRel("userVideos"));
                    links.add(new Link(selfHref + "/login").withRel("login"));
                    links.add(new Link(selfHref + "/name").withRel("name"));
                    links.add(new Link(selfHref + "/password").withRel("password"));
                    links.add(new Link(selfHref + "/description").withRel("description"));
                    Resource<User> resource = new Resource<>(user, links);
                    httpServletResponse.addHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
                    httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
                    PrintWriter writer = httpServletResponse.getWriter();
                    messageConverter.getObjectMapper().writeValue(writer, resource);
                    writer.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Bean
    public AuthenticationFailureHandler loginFailureHandler() {
        return new AuthenticationFailureHandler() {
            @Override
            public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse,
                                                AuthenticationException e) throws IOException, ServletException {
                httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                httpServletResponse.addHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
                httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
                PrintWriter writer = httpServletResponse.getWriter();
                writer.write(e.getMessage());
                writer.flush();
            }
        };
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new LogoutSuccessHandler() {
            @Override
            public void onLogoutSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
                httpServletResponse.setStatus(HttpServletResponse.SC_OK);
                httpServletResponse.addHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
                httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
                PrintWriter writer = httpServletResponse.getWriter();
                writer.write("You've been successfully logged out");
                writer.flush();
            }
        };
    }

    @Bean
    public ApplicationListener<AuthenticationFailureBadCredentialsEvent> authFailure() {
        return new ApplicationListener<AuthenticationFailureBadCredentialsEvent>() {
            @Override
            public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent
                                                   authenticationFailureBadCredentialsEvent) {

            }
        };
    }

    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setSessionAttributeName("_csrf");
        return repository;
    }
}