package com.dataart.practice.cm.data.dao;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

/**
 * Created by aovsyannikov on 8/4/2016.
 */
@Repository
@Profile("RDB/Hibernate")
public class UserDaoJdbcImpl extends UserDaoImpl {
}
