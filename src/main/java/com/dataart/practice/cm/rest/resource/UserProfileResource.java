package com.dataart.practice.cm.rest.resource;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping(value = "/mcm/api/v1/users/{userID}")
public interface UserProfileResource {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUser(@PathVariable(value = "userID") long userID);

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    ResponseEntity<Resource> deleteUser(@PathVariable(value = "userID") long userID,
                                        HttpSession session);

    @RequestMapping(value = "login", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserLogin(@PathVariable(value = "userID") long userID);

    @RequestMapping(value = "login", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> changeUserLogin(@RequestParam(value = "newLogin") String newLogin,
                                             @PathVariable(value = "userID") long userID,
                                             HttpSession session);

    @RequestMapping(value = "name", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserName(@PathVariable(value = "userID") long userID);

    @RequestMapping(value = "name", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> changeUserName(@RequestParam(value = "newName") String newName,
                                            @PathVariable(value = "userID") long userID,
                                            HttpSession session);

    @RequestMapping(value = "description", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserDescription(@PathVariable(value = "userID") long userID);

    @RequestMapping(value = "description", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> changeUserDescription(@RequestParam(value = "newDescription") String newDescription,
                                                   @PathVariable(value = "userID") long userID,
                                                   HttpSession session);

    @RequestMapping(value = "password", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> changeUserPassword(@RequestParam(value = "newPassword") String password,
                                                @RequestParam(value = "confirmPassword") String confirmPassword,
                                                @PathVariable(value = "userID") long userID,
                                                HttpSession session);
}
