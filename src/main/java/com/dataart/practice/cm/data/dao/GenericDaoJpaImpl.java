package com.dataart.practice.cm.data.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public abstract class GenericDaoJpaImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

    protected Class<T> entityClass;

    @Autowired
    protected SessionFactory sessionFactory;

    public GenericDaoJpaImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    @Override
    public T getById(ID mediaId) {
        Session session = sessionFactory.getCurrentSession();
        return session.load(entityClass, mediaId);
    }

    @Override
    public void delete(ID mediaId) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(session.load(entityClass, mediaId));
    }

    @Override
    public void upload(T mediaFile) {
        Session session = sessionFactory.getCurrentSession();
        session.save(mediaFile);
    }

    @Override
    public long countAll() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Long> countAllQuery = sessionFactory.getCriteriaBuilder().createQuery(Long.class);
        countAllQuery.select(builder.count(countAllQuery.from(entityClass)));
        return sessionFactory
                .getCurrentSession()
                .createQuery(countAllQuery)
                .uniqueResult();
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<T> getAllQuery = builder.createQuery(entityClass);
        getAllQuery.select(getAllQuery.from(entityClass));
        return sessionFactory
                .getCurrentSession()
                .createQuery(getAllQuery)
                .list();
    }
}
