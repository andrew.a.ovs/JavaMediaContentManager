package com.dataart.practice.cm.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by aovsyannikov on 8/3/2016.
 */
@NamedQueries({
        @NamedQuery(
                name = "getAllVideos",
                query = "from Video v order by v.date desc"
        ),
        @NamedQuery(
                name = "countAllVideos",
                query = "select count(id) from Video"
        ),
        @NamedQuery(
                name = "getUserVideos",
                query = "from Video where user_id = :userID order by date desc"
        ),
        @NamedQuery(
                name = "countUserVideos",
                query = "select count(id) from Video where user_id = :userID"
        )
})
@Entity(name = "Video")
@Table(name = "videos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Video implements Serializable, Comparable<Video> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long videoID;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @Column(name = "name")
    private String fileName;

    @Column(name = "video")
    private byte[] videoBytes;

    @Transient
    private String driveID;

    private long date;

    private int size;

    private String extension;

    public Video() {
    }

    public Video(MediaMetadata metadata) {
        this.videoID = metadata.getMediaId();
        this.user = metadata.getUser();
        this.fileName = metadata.getFileName();
        this.driveID = metadata.getDriveID();
        this.date = metadata.getDate();
        this.size = metadata.getSize();
        this.extension = metadata.getExtension();
    }

    public Video(byte[] videoBytes, User user, String fileName, long date, int size, String extension) {
        this.videoBytes = videoBytes;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    public Video(long videoID, byte[] videoBytes, User user, String fileName, long date, int size, String extension) {
        this.videoID = videoID;
        this.videoBytes = videoBytes;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    public Video(long videoID, String driveID, User user, String fileName, long date, int size, String extension) {
        this.videoID = videoID;
        this.driveID = driveID;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    @Override
    public int compareTo(Video o) {
        return (int) (o.date - this.date);
    }

    @Override
    public String toString() {
        return "Filename: " + getFileName() + " Size: " + getSize();
    }

    @Override
    public int hashCode() {
        int result = 17;
        int hash = Arrays.hashCode(this.videoBytes) + this.fileName.hashCode();
        result = 31 * result + hash;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Video)) {
            return false;
        } else {
            Video v = (Video) obj;
            return (Arrays.equals(this.videoBytes, v.getVideoBytes()) && this.fileName.equals(v.getFileName()));
        }
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public long getVideoID() {
        return videoID;
    }

    public void setVideoID(long photoID) {
        this.videoID = photoID;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public byte[] getVideoBytes() {
        return videoBytes;
    }

    public void setVideoBytes(byte[] videoBytes) {
        this.videoBytes = videoBytes;
    }

    public String getDriveID() {
        return driveID;
    }

    public void setDriveID(String driveID) {
        this.driveID = driveID;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size / 1024;
    }


}
