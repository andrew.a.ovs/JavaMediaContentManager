package com.dataart.practice.cm.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aovsyannikov on 8/11/2016.
 */
@Entity(name = "Role")
@Table(name = "roles")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Role implements Serializable {

    @Id
    @Column(name = "id", updatable = false)
    @JsonIgnore
    private long id;

    @Column(name = "role_name", updatable = false)
    private String roleName;

    @OneToMany(mappedBy = "role", targetEntity = User.class)
    @JsonIgnore
    private List<User> usersList;

    public long getId() {
        return id;
    }

    public String getRoleName() {
        return roleName;
    }

    public Role() {
    }

    public Role(long id, String roleName) {
        this.id = id;
        this.roleName = roleName;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.roleName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Role)) {
            return false;
        } else {
            Role r = (Role) obj;
            return this.roleName.equals(r.getRoleName());
        }
    }

    @Override
    public String toString() {
        return this.roleName;
    }
}

