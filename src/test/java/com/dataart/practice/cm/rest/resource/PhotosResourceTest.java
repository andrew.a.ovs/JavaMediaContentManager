package com.dataart.practice.cm.rest.resource;

import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.service.UserService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by aovsyannikov on 9/23/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class PhotosResourceTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    private User admin;

    private User user;

    private User anotherUser;

    @Value("${hostname}")
    private String hostname;

    @Value("${jsonContentType}")
    private String json;

    @Value("${originHeader}")
    private String origin;

    @Value("${contentDispositionHeader}")
    private String contentDisposition;

    @Value("${contentDispositionTemplate}")
    private String cdTemplate;

    @Value("${photosRoot}")
    private String photos;

    @Value("${getAllPhotos}")
    private String testStringGetAllPhotos;

    @Value("${invalidPaginationParams}")
    private String testStringInvalidPagination;

    @Value("${getPhotosCount}")
    private String testStringGetPhotosCount;

    @Value("${getPhoto}")
    private String testStringGetPhoto;

    @Value("${unauthorizedPhotoRemovalAttempt}")
    private String testStringUnauthPhotoRemoval;

    @Value("${photoRemovalSuccess}")
    private String testStringPhotoRemovalSuccess;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        admin = userService.getUserById(1L);
        user = userService.getUserById(2L);
        anotherUser = userService.getUserById(3L);
    }

    @Test
    public void test0GetAllPhotos() throws Exception {
        assertTrue(mockMvc
                .perform(get(photos)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetAllPhotos)
        );
        assertTrue(mockMvc
                .perform(get(photos)
                        .header(origin, hostname)
                        .param("page", "1")
                        .param("pageSize", "7"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetAllPhotos)
        );
        assertTrue(mockMvc
                .perform(get(photos)
                        .header(origin, hostname)
                        .param("page", "2")
                        .param("pageSize", "7"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringInvalidPagination)
        );
    }

    @Test
    public void test1GetPhotosCount() throws Exception {
        assertTrue(mockMvc
                .perform(get(photos + "/count")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetPhotosCount)
        );
    }

    @Test
    public void test2GetPhoto() throws Exception {
        assertTrue(mockMvc
                .perform(get(photos + "/1")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetPhoto)
        );
        mockMvc
                .perform(get(photos + "/666")
                        .header(origin, hostname))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(json));
    }

    @Test
    public void test3DeletePhoto() throws Exception {
        assertTrue(mockMvc
                .perform(delete(photos + "/5")
                        .sessionAttr("user", user)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthPhotoRemoval)
        );
        assertTrue(mockMvc
                .perform(delete(photos + "/5")
                        .sessionAttr("user", anotherUser)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPhotoRemovalSuccess)
        );
        assertTrue(mockMvc
                .perform(delete(photos + "/6")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPhotoRemovalSuccess)
        );
    }

    @Test
    public void test4DownloadPhoto() throws Exception {
        mockMvc
                .perform(get(photos + "/1/download")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType("image/jpeg"))
                .andExpect(header().string(contentDisposition, cdTemplate + "testPhoto1")
                );
    }


}
