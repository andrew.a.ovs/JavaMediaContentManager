package com.dataart.practice.cm.aspects;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.data.model.Video;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by aovsyannikov on 8/5/2016.
 */
@Aspect
@Component
public class LoggingAspect {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private static final String REGISTRATION_SUCCESS = "Successful registration of the " +
            "new user with following credentials: ";
    private static final String REGISTRATION_FAILURE = "Failed attempt to register " +
            "new user with following credentials: ";

    private static final String LOGIN_SUCCESS = "Successful login of user with following credentials: ";
    private static final String LOGIN_FAILURE = "Unsuccessful login attempt";

    private static final String USER_REMOVAL = "Successful removal of user with ID: ";
    private static final String USER_REMOVAL_FAILURE = "Failed removing user with ID: ";

    private static final String PHOTO_REMOVAL_SUCCESS = "Successful removal of photo with ID: ";
    private static final String PHOTO_REMOVAL_FAILURE = "Failed removing photo with ID: ";

    private static final String VIDEO_REMOVAL_SUCCESS = "Successful removal of video with ID: ";
    private static final String VIDEO_REMOVAL_FAILURE = "Failed removing video with ID: ";

    private static final String PHOTO_UPLOAD_SUCCESS = "Successful upload of photo: ";
    private static final String PHOTO_UPLOAD_FAILURE = "Failed to upload photo: ";

    private static final String VIDEO_UPLOAD_SUCCESS = "Successful upload of photo: ";
    private static final String VIDEO_UPLOAD_FAILURE = "Failed to upload photo: ";

    private static final String PHOTO_DOWNLOAD_SUCCESS = "Successful download of photo with ID: ";
    private static final String PHOTO_DOWNLOAD_FAILURE = "Failed downloading photo with ID: ";

    private static final String VIDEO_DOWNLOAD_SUCCESS = "Successful download of video with ID: ";
    private static final String VIDEO_DOWNLOAD_FAILURE = "Failed downloading video with ID: ";

    private static final String LOGIN_CHANGE_SUCCESS = "User under userID: {} " +
            "has successfully changed his login to: {}";
    private static final String LOGIN_CHANGE_FAILURE = "User under userID: {} " +
            "has failed to change his login to: {}";

    private static final String USERNAME_CHANGE_SUCCESS = "User under userID: {} " +
            "has successfully changed his username to: {}";
    private static final String USERNAME_CHANGE_FAILURE = "User under userID: {} " +
            "has failed to change his username to: {}";

    private static final String DESCRIPTION_CHANGE_SUCCESS = "User under userID: {} " +
            "has successfully changed his description to: {}";
    private static final String DESCRIPTION_CHANGE_FAILURE = "User under userID: {} " +
            "has failed to change his description to: {}";

    private static final String PASSWORD_CHANGE_SUCCESS = "User under userID: {}" +
            " has successfully changed his password";
    private static final String PASSWORD_CHANGE_FAILURE = "User under userID: {}" +
            " has failed to change his password";


    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.registerUser" +
            "(com.dataart.practice.cm.data.model.User)) " +
            "&& args(user)")
    public void register(User user) {
    }

    @AfterReturning(value = "register(user)")
    public void registration(User user) {
        LOGGER.info(REGISTRATION_SUCCESS + user.toString());
    }

    @AfterThrowing(value = "register(user)", throwing = "exc")
    public void registrationException(User user, Throwable exc) {
        LOGGER.error(REGISTRATION_FAILURE + user.toString() + ", an exception was thrown: " + exc);
    }

    @Pointcut("execution(com.dataart.practice.cm.data.model.User " +
            "com.dataart.practice.cm.data.dao.UserDao.getUserByLogin(..)) ")
    public void login() {
    }

    @Pointcut("execution(void org.springframework.context.ApplicationListener.onApplicationEvent" +
            "(org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent))" +
            " && args(authenticationFailureBadCredentialsEvent)")
    public void loginFailure(AuthenticationFailureBadCredentialsEvent authenticationFailureBadCredentialsEvent) {
    }

    @AfterReturning(value = "loginFailure(event)")
    public void logFailure(AuthenticationFailureBadCredentialsEvent event) {
        LOGGER.error(LOGIN_FAILURE + " with login: " + event.getAuthentication().getPrincipal()
                + " , exception thrown: " + event.getException());
    }

    @AfterReturning(value = "login()", returning = "user")
    public void loginSuccess(User user) {
        LOGGER.info(LOGIN_SUCCESS + user.toString());
    }

    @AfterThrowing(value = "login()", throwing = "exc")
    public void loginExc(Throwable exc) {
        LOGGER.error(LOGIN_FAILURE + ", an exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.deleteUser(long)) && args(userID)")
    public void userRemoval(long userID) {
    }

    @AfterReturning(value = "userRemoval(userID)")
    public void userRemoved(long userID) {
        LOGGER.info(USER_REMOVAL + userID);
    }

    @AfterThrowing(value = "userRemoval(userID)", throwing = "exc")
    public void userRemovalExc(long userID, Throwable exc) {
        LOGGER.error(USER_REMOVAL_FAILURE + userID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.service.MediaService.deletePhoto(long)) && args(photoID)")
    public void deletePhoto(long photoID) {
    }

    @AfterReturning(value = "deletePhoto(photoID)")
    public void deletePhotoSuccess(long photoID) {
        LOGGER.info(PHOTO_REMOVAL_SUCCESS + photoID);
    }

    @AfterThrowing(value = "deletePhoto(photoID)", throwing = "exc")
    public void deletePhotoFailure(long photoID, Throwable exc) {
        LOGGER.error(PHOTO_REMOVAL_FAILURE + photoID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.service.MediaService.deleteVideo(long)) && args(videoID)")
    public void deleteVideo(long videoID) {
    }

    @AfterReturning(value = "deleteVideo(videoID)")
    public void deleteVideoSuccess(long videoID) {
        LOGGER.info(VIDEO_REMOVAL_SUCCESS + videoID);
    }

    @AfterThrowing(value = "deleteVideo(videoID)", throwing = "exc")
    public void deleteVideoFailure(long videoID, Throwable exc) {
        LOGGER.error(VIDEO_REMOVAL_FAILURE + videoID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm..service.MediaService.uploadPhoto" +
            "(com.dataart.practice.cm.data.model.Photo))" +
            " && args(photo)")
    public void uploadPhoto(Photo photo) {
    }

    @AfterReturning(value = "uploadPhoto(photo)")
    public void uploadPhotoResult(Photo photo) {
        LOGGER.info(PHOTO_UPLOAD_SUCCESS + photo.toString());
    }

    @AfterThrowing(value = "uploadPhoto(photo)", throwing = "exc")
    public void uploadPhotoFailure(Photo photo, Throwable exc) {
        LOGGER.error(PHOTO_UPLOAD_FAILURE + photo.toString() + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.service.MediaService.uploadVideo" +
            "(com.dataart.practice.cm.data.model.Video))" +
            " && args(video)")
    public void uploadVideo(Video video) {
    }

    @AfterReturning(value = "uploadVideo(video)")
    public void uploadVideoResult(Video video) {
        LOGGER.info(VIDEO_UPLOAD_SUCCESS + video.toString());
    }

    @AfterThrowing(value = "uploadVideo(video)", throwing = "exc")
    public void uploadVideoFailure(Video video, Throwable exc) {
        LOGGER.error(VIDEO_UPLOAD_FAILURE + video.toString() + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.rest.resource.impl.UserMediaResourceImpl.downloadUserPhotoById" +
            "(long, javax.servlet.http.HttpServletResponse)) && args(photoID, req)")
    public void downloadPhoto(long photoID, HttpServletResponse req) {
    }

    @AfterReturning(value = "downloadPhoto(photoID, req)")
    public void downloadPhotoSuccess(long photoID, HttpServletResponse req) {
        LOGGER.info(PHOTO_DOWNLOAD_SUCCESS + photoID);
    }

    @AfterThrowing(value = "downloadPhoto(photoID, req)", throwing = "exc")
    public void downloadPhotoFailure(long photoID, HttpServletResponse req, Throwable exc) {
        LOGGER.error(PHOTO_DOWNLOAD_FAILURE + photoID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.rest.resource.impl.UserMediaResourceImpl.downloadUserVideoById" +
            "(long, javax.servlet.http.HttpServletResponse)) && args(videoID, req)")
    public void downloadVideo(long videoID, HttpServletResponse req) {
    }

    @AfterReturning(value = "downloadVideo(videoID, req)")
    public void downloadVideoSuccess(long videoID, HttpServletResponse req) {
        LOGGER.info(VIDEO_DOWNLOAD_SUCCESS + videoID);
    }

    @AfterThrowing(value = "downloadVideo(videoID, req)", throwing = "exc")
    public void downloadVideoFailure(long videoID, HttpServletResponse req, Throwable exc) {
        LOGGER.error(VIDEO_DOWNLOAD_FAILURE + videoID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.rest.resource.impl.PhotosResourceImpl.downloadPhotoById" +
            "(long, javax.servlet.http.HttpServletResponse)) && args(photoID, req)")
    public void downloadPhotoGen(long photoID, HttpServletResponse req) {
    }

    @AfterReturning(value = "downloadPhotoGen(photoID, req)")
    public void downloadPhotoSuccessGen(long photoID, HttpServletResponse req) {
        LOGGER.info(PHOTO_DOWNLOAD_SUCCESS + photoID);
    }

    @AfterThrowing(value = "downloadPhotoGen(photoID, req)", throwing = "exc")
    public void downloadPhotoFailureGen(long photoID, HttpServletResponse req, Throwable exc) {
        LOGGER.error(PHOTO_DOWNLOAD_FAILURE + photoID + ", exception was thrown: " + exc);
    }

    @Pointcut("execution(void com.dataart.practice.cm.rest.resource.impl.VideosResourceImpl.downloadVideoById" +
            "(long, javax.servlet.http.HttpServletResponse)) && args(videoID, req)")
    public void downloadVideoGen(long videoID, HttpServletResponse req) {
    }

    @AfterReturning(value = "downloadVideoGen(videoID, req)")
    public void downloadVideoSuccessGen(long videoID, HttpServletResponse req) {
        LOGGER.info(VIDEO_DOWNLOAD_SUCCESS + videoID);
    }

    @AfterThrowing(value = "downloadVideo(videoID, req)", throwing = "exc")
    public void downloadVideoFailureGen(long videoID, HttpServletResponse req, Throwable exc) {
        LOGGER.error(VIDEO_DOWNLOAD_FAILURE + videoID + ", exception was thrown: " + exc);
    }


    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.setNewLogin(long, String)) " +
            "&& args(userID, newLogin)")
    public void changeLogin(long userID, String newLogin) {
    }

    @AfterReturning(value = "changeLogin(userID, newLogin)")
    public void changeLoginSuccess(long userID, String newLogin) {
        LOGGER.info(LOGIN_CHANGE_SUCCESS, userID, newLogin);
    }

    @AfterThrowing(value = "changeLogin(userID, newLogin)", throwing = "exc")
    public void changeLoginFailure(long userID, String newLogin, Throwable exc) {
        LOGGER.error(LOGIN_CHANGE_FAILURE, userID, newLogin + ", an exception was thrown: " + exc);
    }


    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.setNewUsername(long, String)) " +
            "&& args(userID, newUsername)")
    public void changeUsername(long userID, String newUsername) {
    }

    @AfterReturning(value = "changeUsername(userID, newUsername)")
    public void changeUsernameSuccess(long userID, String newUsername) {
        LOGGER.info(USERNAME_CHANGE_SUCCESS, userID, newUsername);
    }

    @AfterThrowing(value = "changeUsername(userID, newUsername)", throwing = "exc")
    public void changeUsernameFailure(long userID, String newUsername, Throwable exc) {
        LOGGER.error(USERNAME_CHANGE_FAILURE, userID, newUsername + ", an exception was thrown: " + exc);
    }


    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.setNewDescription(long, String)) " +
            "&& args(userID, newDescription)")
    public void changeDescription(long userID, String newDescription) {
    }

    @AfterReturning(value = "changeDescription(userID, newDescription)")
    public void changeDescriptionSuccess(long userID, String newDescription) {
        LOGGER.info(DESCRIPTION_CHANGE_SUCCESS, userID, newDescription);
    }

    @AfterThrowing(value = "changeDescription(userID, newDescription)", throwing = "exc")
    public void changeDescriptionFailure(long userID, String newDescription, Throwable exc) {
        LOGGER.error(DESCRIPTION_CHANGE_FAILURE, userID, newDescription + ", an exception was thrown: " + exc);
    }


    @Pointcut("execution(void com.dataart.practice.cm.data.dao.UserDao.setNewPassword(long, String)) " +
            "&& args(userID, newPassword)")
    public void changePassword(long userID, String newPassword) {
    }

    @AfterReturning(value = "changePassword(userID, newPassword)")
    public void changePasswordSuccess(long userID, String newPassword) {
        LOGGER.info(PASSWORD_CHANGE_SUCCESS, userID, newPassword);
    }

    @AfterThrowing(value = "changePassword(userID, newPassword)", throwing = "exc")
    public void changePasswordFailure(long userID, String newPassword, Throwable exc) {
        LOGGER.error(PASSWORD_CHANGE_FAILURE, userID + ", an exception was thrown: " + exc);
    }

    @Pointcut("execution(org.springframework.http.ResponseEntity" +
            " com.dataart.practice.cm.rest.resource.impl.UserMediaResourceImpl.uploadPhoto" +
            "(long, org.springframework.web.multipart.MultipartFile, javax.servlet.http.HttpSession))" +
            "&& args(userID, file, session)")
    public void uploadFail(long userID, MultipartFile file, HttpSession session) {
    }

    @AfterReturning(value = "uploadFail(userID, file, session)", returning = "respEntity")
    public void photoUploadFail(long userID, MultipartFile file, HttpSession session, ResponseEntity respEntity) {
        if (!respEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            LOGGER.error("Photo upload by user with ID:" + userID + " failed with message: " +
                    ((Resource) respEntity.getBody()).getContent());
        }
    }

    @Pointcut("execution(org.springframework.http.ResponseEntity" +
            " com.dataart.practice.cm.rest.resource.impl.UserMediaResourceImpl.uploadVideo" +
            "(long, org.springframework.web.multipart.MultipartFile, javax.servlet.http.HttpSession))" +
            "&& args(userID, file, session)")
    public void uploadVideoFail(long userID, MultipartFile file, HttpSession session) {
    }

    @AfterReturning(value = "uploadVideoFail(userID, file, session)", returning = "respEntity")
    public void videoUploadFail(long userID, MultipartFile file, HttpSession session, ResponseEntity respEntity) {
        if (!respEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            LOGGER.error("Video upload by user with ID:" + userID + " failed with message: " +
                    ((Resource) respEntity.getBody()).getContent());
        }
    }

    @Pointcut("execution(org.springframework.http.ResponseEntity" +
            " com.dataart.practice.cm.rest.resource.impl.UsersResourceImpl.registerUser" +
            "(String, String, String, String, String))" +
            "&& args(login, password, confirmPassword, name, description)")
    public void userRegistrationFail(String login, String password, String confirmPassword,
                                     String name, String description) {
    }

    @AfterReturning(value = "userRegistrationFail(login, password, confirmPassword, name, description)",
            returning = "respEntity")
    public void userRegistrationFailure(String login, String password, String confirmPassword,
                                        String name, String description, ResponseEntity respEntity) {
        if (!respEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            LOGGER.error("User registration with following login: " + login + ", failed with message: " +
                    ((Resource) respEntity.getBody()).getContent());
        }
    }

    @Pointcut("execution(org.springframework.http.ResponseEntity" +
            " com.dataart.practice.cm.rest.resource.impl.UsersResourceImpl.addUser" +
            "(String, String, String, String, String, javax.servlet.http.HttpSession))" +
            "&& args(login, password, confirmPassword, name, description, session)")
    public void userAdditionFail(String login, String password, String confirmPassword,
                                 String name, String description, HttpSession session) {
    }

    @AfterReturning(value = "userAdditionFail(login, password, confirmPassword, name, description, session)",
            returning = "respEntity")
    public void userAdditionFailure(String login, String password, String confirmPassword,
                                    String name, String description, HttpSession session,
                                    ResponseEntity respEntity) {
        if (!respEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            LOGGER.error("User registration with following login: " + login + ", failed with message: " +
                    ((Resource) respEntity.getBody()).getContent());
        }
    }
}
