package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.Video;
import org.hibernate.Session;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("RDB/Hibernate")
public class VideoDaoJpaImpl extends GenericDaoJpaImpl<Video, Long> implements VideoDao {

    @Override
    public List<Video> getUserMedia(Long userID) {
        Session session = sessionFactory.getCurrentSession();
        return session
                .getNamedQuery("getUserVideos")
                .setParameter("userID", userID)
                .list();
    }

    @Override
    public long countUserMedia(Long userID) {
        Session session = sessionFactory.getCurrentSession();
        return (Long) session
                .getNamedQuery("countUserVideos")
                .setParameter("userID", userID)
                .uniqueResult();
    }
}
