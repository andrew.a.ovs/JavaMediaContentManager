package com.dataart.practice.cm.rest.resource;

import com.dataart.practice.cm.data.model.Role;
import com.dataart.practice.cm.data.model.User;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class UsersResourceTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private User admin;

    private User user;

    @Value("${hostname}")
    private String hostname;

    @Value("${jsonContentType}")
    private String json;

    @Value("${originHeader}")
    private String origin;

    @Value("${contentDispositionHeader}")
    private String contentDisposition;

    @Value("${contentDispositionTemplate}")
    private String cdTemplate;

    @Value("${usersRoot}")
    private String users;

    @Value("${getAllUsers}")
    private String testStringGetAllUsers;

    @Value("${getUsersCount}")
    private String testStringGetUsersCount;

    @Value("${registerUser}")
    private String testStringRegisterUser;

    @Value("${emptyLogin}")
    private String testStringEmptyLogin;

    @Value("${takenLogin}")
    private String testStringTakenLogin;

    @Value("${passwordMismatch}")
    private String testStringPasswordMismatch;

    @Value("${unauthorizedUserAdditionAttempt}")
    private String testStringUserAddition;

    @Value("${invalidPaginationParams}")
    private String testStringInvalidPagination;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        admin = new User("admin", "admin", "username", "description", new Role(1, "ADMIN"));
        user = new User("user", "user", "username", "description", new Role(0, "USER"));
    }

    @Test
    public void test0GetAllUsers() throws Exception {
        assertTrue(mockMvc
                .perform(get(users)
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetAllUsers));
        assertTrue(mockMvc
                .perform(get(users)
                        .sessionAttr("user", admin)
                        .header(origin, hostname)
                        .param("page", "1")
                        .param("pageSize", "5"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetAllUsers));
        assertTrue(mockMvc
                .perform(get(users)
                        .sessionAttr("user", admin)
                        .header(origin, hostname)
                        .param("page", "2")
                        .param("pageSize", "5"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringInvalidPagination));
        mockMvc
                .perform(get(users)
                        .sessionAttr("user", user)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json));
    }

    @Test
    public void test1GetUsersCount() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/count")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUsersCount));
    }

    @Test
    public void test2RegisterUser() throws Exception {
        assertTrue(mockMvc
                .perform(post(users + "/registration")
                        .header(origin, hostname)
                        .param("login", "qwerty")
                        .param("password", "z")
                        .param("confirmPassword", "z"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringRegisterUser));
        assertTrue(mockMvc
                .perform(post(users + "/registration")
                        .header(origin, hostname)
                        .param("login", "")
                        .param("password", "z")
                        .param("confirmPassword", "z"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringEmptyLogin));
        assertTrue(mockMvc
                .perform(post(users + "/registration")
                        .header(origin, hostname)
                        .param("login", "admin")
                        .param("password", "z")
                        .param("confirmPassword", "z"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringTakenLogin));
        assertTrue(mockMvc
                .perform(post(users + "/registration")
                        .header(origin, hostname)
                        .param("login", "qwerty")
                        .param("password", "z")
                        .param("confirmPassword", "q"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPasswordMismatch));
        mockMvc
                .perform(post(users + "/registration")
                        .header(origin, hostname)
                        .param("login", "qwerty")
                        .param("confirmPassword", "q"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void test3AddUser() throws Exception {
        assertTrue(mockMvc
                .perform(post(users)
                        .sessionAttr("user", user)
                        .header(origin, hostname)
                        .param("login", "ytrewq")
                        .param("password", "z")
                        .param("confirmPassword", "z"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUserAddition));
        mockMvc
                .perform(post(users)
                        .sessionAttr("user", admin)
                        .header(origin, hostname)
                        .param("login", "asdfgh")
                        .param("password", "z")
                        .param("confirmPassword", "z"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json));
    }
}
