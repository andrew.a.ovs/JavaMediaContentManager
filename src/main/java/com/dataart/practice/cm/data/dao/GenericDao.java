package com.dataart.practice.cm.data.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public interface GenericDao<T, ID extends Serializable> {

    T getById(ID mediaId) throws Exception;

    void delete(ID mediaId) throws Exception;

    void upload(T mediaFile) throws Exception;

    long countAll() throws Exception;

    List<T> getAll() throws Exception;

    long countUserMedia(ID userId) throws Exception;

    List<T> getUserMedia(ID userId) throws Exception;

}
