package com.dataart.practice.cm.rest.resource;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping(value = "/mcm/api/v1/users")
public interface UsersResource {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Resource>> getUsers(@RequestParam(value = "page", required = false, defaultValue = "0")
                                                    int page,
                                            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                                                    int pageSize,
                                            HttpSession session);

    @RequestMapping(value = "count", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUsersCount();

    @RequestMapping(value = "registration", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> registerUser(@RequestParam(name = "login") String login,
                                          @RequestParam(name = "password") String password,
                                          @RequestParam(name = "confirmPassword") String confirmPassword,
                                          @RequestParam(name = "name", required = false) String name,
                                          @RequestParam(name = "description", required = false) String description);

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> addUser(@RequestParam(name = "login") String login,
                                     @RequestParam(name = "password") String password,
                                     @RequestParam(name = "confirmPassword") String confirmPassword,
                                     @RequestParam(name = "name", required = false) String name,
                                     @RequestParam(name = "description", required = false) String description,
                                     HttpSession session);
}
