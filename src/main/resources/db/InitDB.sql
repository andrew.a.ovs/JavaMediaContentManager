DROP TABLE IF EXISTS photos CASCADE;

DROP TABLE IF EXISTS roles CASCADE;

DROP TABLE IF EXISTS users CASCADE;

DROP TABLE IF EXISTS photos_data CASCADE;

DROP TABLE IF EXISTS videos_data CASCADE;

CREATE TABLE roles
(
  id bigint NOT NULL,
  role_name character varying(255),
  CONSTRAINT roles_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

INSERT INTO roles (id, role_name) VALUES (0, 'USER');

INSERT INTO roles (id, role_name) VALUES (1, 'ADMIN');

CREATE TABLE users
(
  id serial NOT NULL,
  description character varying(255),
  enabled integer,
  login character varying(255),
  password character varying(255),
  name character varying(255),
  role_id bigint,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT fkp56c1712k691lhsyewcssf40f FOREIGN KEY (role_id)
  REFERENCES roles (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_sx468g52bpetvlad2j9y0lptc UNIQUE (login)
)
WITH (
OIDS=FALSE
);

CREATE TABLE photos
(
  id serial NOT NULL,
  date bigint NOT NULL,
  extension character varying(255),
  name character varying(255),
  image bytea,
  size integer NOT NULL,
  user_id integer,
  CONSTRAINT photos_pkey PRIMARY KEY (id),
  CONSTRAINT fknm381g1ktlpsorbtpco2ljhuv FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

CREATE TABLE photos_data
(
  id bigint NOT NULL,
  date bigint,
  extension character varying(255),
  name character varying(255),
  size integer,
  user_id bigint,
  drive_id character varying(255) NOT NULL,
  CONSTRAINT photos_data_pkey PRIMARY KEY (id)
);

CREATE TABLE videos_data
(
  id bigint NOT NULL,
  date bigint,
  extension character varying(255),
  name character varying(255),
  size integer,
  user_id bigint,
  drive_id character varying(255) NOT NULL,
  CONSTRAINT videos_data_pkey PRIMARY KEY (id)
);

INSERT INTO users (name, login, password, role_id, description, enabled) VALUES ('John Doe', 'admin', '3d3784f408debc3f6f42fc9358ca3068a3571e332648b9933ab25aa9bcec359ef42a66f6c9e245e2', 1, 'default administrator account', 1);

