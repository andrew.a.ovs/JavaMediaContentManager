package com.dataart.practice.cm.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by aovsyannikov on 8/3/2016.
 */
@NamedQueries({
        @NamedQuery(
                name = "getAllPhotos",
                query = "from Photo p order by p.date desc"
        ),
        @NamedQuery(
                name = "countAllPhotos",
                query = "select count(id) from Photo"
        ),
        @NamedQuery(
                name = "getUserPhotos",
                query = "from Photo where user_id = :userID order by date desc"
        ),
        @NamedQuery(
                name = "countUserPhotos",
                query = "select count(id) from Photo where user_id = :userID"
        )
})
@Entity(name = "Photo")
@Table(name = "photos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Photo implements Serializable, Comparable<Photo> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long photoID;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @Column(name = "name")
    private String fileName;

    @Column(name = "image")
    private byte[] imageBytes;

    @Transient
    @JsonIgnore
    private String driveID;

    private long date;

    private int size;

    private String extension;

    public Photo() {
    }

    public Photo(MediaMetadata metadata) {
        this.photoID = metadata.getMediaId();
        this.user = metadata.getUser();
        this.fileName = metadata.getFileName();
        this.driveID = metadata.getDriveID();
        this.date = metadata.getDate();
        this.size = metadata.getSize();
        this.extension = metadata.getExtension();
    }

    public Photo(byte[] imageBytes, User user, String fileName, long date, int size, String extension) {
        this.imageBytes = imageBytes;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    public Photo(long photoID, byte[] imageBytes, User user, String fileName, long date, int size, String extension) {
        this.photoID = photoID;
        this.imageBytes = imageBytes;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    public Photo(long photoID, String driveID, User user, String fileName, long date, int size, String extension) {
        this.photoID = photoID;
        this.driveID = driveID;
        this.user = user;
        this.fileName = fileName;
        this.size = size / 1024;
        this.date = date;
        this.extension = extension;
    }

    @Override
    public int compareTo(Photo o) {
        return (int) (o.date - this.date);
    }

    @Override
    public String toString() {
        return "Filename: " + getFileName() + " Size: " + getSize();
    }

    @Override
    public int hashCode() {
        int result = 17;
        int hash = Arrays.hashCode(this.imageBytes) + this.fileName.hashCode();
        result = 31 * result + hash;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Photo)) {
            return false;
        } else {
            Photo p = (Photo) obj;
            return (Arrays.equals(this.imageBytes, p.getImageBytes()) && this.fileName.equals(p.getFileName()));
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public long getPhotoID() {
        return photoID;
    }

    public void setPhotoID(long photoID) {
        this.photoID = photoID;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getDriveID() {
        return driveID;
    }

    public void setDriveID(String driveID) {
        this.driveID = driveID;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size / 1024;
    }


}
