package com.dataart.practice.cm.rest.resource;

import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.service.UserService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aovsyannikov on 9/23/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class UserProfileResourceImplTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    private User admin;

    private User user;

    private User anotherUser;

    @Value("${hostname}")
    private String hostname;

    @Value("${jsonContentType}")
    private String json;

    @Value("${originHeader}")
    private String origin;

    @Value("${contentDispositionHeader}")
    private String contentDisposition;

    @Value("${contentDispositionTemplate}")
    private String cdTemplate;

    @Value("${usersRoot}")
    private String users;

    @Value("${getUser}")
    private String testStringGetUser;

    @Value("${unauthUserRemovalAttempt}")
    private String testStringUnauthUserRemovalAttempt;

    @Value("${userRemovalSuccess}")
    private String testStringUserRemovalSuccess;

    @Value("${getUserLogin}")
    private String testStringGetUserLogin;

    @Value("${unauthLoginChangeAttempt}")
    private String testStringUnauthLoginChangeAttempt;

    @Value("${takenLoginFailure}")
    private String testStringTakenLoginFailure;

    @Value("${loginChangeSuccess}")
    private String testStringLoginChangeSuccess;

    @Value("${getUsername}")
    private String testStringGetUsername;

    @Value("${unauthUsernameChangeAttempt}")
    private String testStringUnauthUsernameChangeAttempt;

    @Value("${usernameChangeSuccess}")
    private String testStringUsernameChangeSuccess;

    @Value("${getDescription}")
    private String testStringGetDescription;

    @Value("${unauthDescriptionChangeAttempt}")
    private String testStringUnauthDescriptionChangeAttempt;

    @Value("${descriptionChangeSuccess}")
    private String testStringDescriptionChangeSuccess;

    @Value("${unauthPasswordChangeAttempt}")
    private String testStringUnauthPasswordChangeAttempt;

    @Value("${passwordMismatchFailure}")
    private String testStringPasswordMismatchFailure;

    @Value("${passwordChangeSuccess}")
    private String testStringPasswordChangeSuccess;


    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        admin = userService.getUserById(1L);
        user = userService.getUserById(2L);
        anotherUser = userService.getUserById(3L);
    }

    @Test
    public void test0GetUser() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/1")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUser)
        );
        mockMvc
                .perform(get(users + "/666")
                        .header(origin, hostname))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(json));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void test1DeleteUser() throws Exception {
        assertTrue(mockMvc
                .perform(delete(users + "/2")
                        .sessionAttr("user", anotherUser)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthUserRemovalAttempt)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/1")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthUserRemovalAttempt)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/2")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUserRemovalSuccess)
        );
    }

    @Test
    public void test2GetUserLogin() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/login")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserLogin)
        );
    }

    @Test
    public void test3ChangeUserLogin() throws Exception {
        assertTrue(mockMvc
                .perform(post(users + "/3/login")
                        .sessionAttr("user", user)
                        .param("newLogin", "newTestLogin")
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthLoginChangeAttempt)
        );
        assertTrue(mockMvc
                .perform(post(users + "/2/login")
                        .sessionAttr("user", user)
                        .param("newLogin", "admin")
                        .header(origin, hostname))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringTakenLoginFailure)
        );
        assertTrue(mockMvc
                .perform(post(users + "/2/login")
                        .sessionAttr("user", user)
                        .param("newLogin", "newTestLogin")
                        .header(origin, hostname))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringLoginChangeSuccess)
        );
    }

    @Test
    public void test4GetUsername() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/name")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUsername)
        );
    }

    @Test
    public void test5ChangeUsername() throws Exception {
        assertTrue(mockMvc
                .perform(post(users + "/3/name")
                        .sessionAttr("user", user)
                        .param("newName", "newTestName")
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthUsernameChangeAttempt)
        );
        assertTrue(mockMvc
                .perform(post(users + "/2/name")
                        .sessionAttr("user", user)
                        .param("newName", "newTestName")
                        .header(origin, hostname))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUsernameChangeSuccess)
        );
    }

    @Test
    public void test6GetDescription() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/description")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetDescription)
        );
    }

    @Test
    public void test7ChangeDescription() throws Exception {
        assertTrue(mockMvc
                .perform(post(users + "/2/description")
                        .sessionAttr("user", anotherUser)
                        .param("newDescription", "newTestDescription")
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthDescriptionChangeAttempt)
        );
        assertTrue(mockMvc
                .perform(post(users + "/3/description")
                        .sessionAttr("user", anotherUser)
                        .param("newDescription", "newTestDescription")
                        .header(origin, hostname))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringDescriptionChangeSuccess)
        );
    }

    @Test
    public void test8ChangePassword() throws Exception {
        assertTrue(mockMvc
                .perform(post(users + "/3/password")
                        .sessionAttr("user", user)
                        .param("newPassword", "newTestPassword")
                        .param("confirmPassword", "newTestPassword")
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthPasswordChangeAttempt)
        );
        assertTrue(mockMvc
                .perform(post(users + "/2/password")
                        .sessionAttr("user", user)
                        .param("newPassword", "newTestPassword")
                        .param("confirmPassword", "newPassword")
                        .header(origin, hostname))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPasswordMismatchFailure)
        );
        assertTrue(mockMvc
                .perform(post(users + "/2/password")
                        .sessionAttr("user", user)
                        .param("newPassword", "newTestPassword")
                        .param("confirmPassword", "newTestPassword")
                        .header(origin, hostname))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPasswordChangeSuccess)
        );
    }


}
