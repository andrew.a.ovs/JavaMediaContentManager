package com.dataart.practice.cm.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aovsyannikov on 7/20/2016.
 */
@NamedQueries({
        @NamedQuery(
                name = "getAllUsers",
                query = "from User order by id, role_id DESC"
        ),
        @NamedQuery(
                name = "deleteUser",
                query = "delete from User u where u.userID = :userID"
        ),
        @NamedQuery(
                name = "deleteUserPhotos",
                query = "delete from Photo p where p.user.userID = :userID"
        ),
        @NamedQuery(
                name = "deleteUserVideos",
                query = "delete from Video v where v.user.userID = :userID"
        ),
        @NamedQuery(
                name = "countAll",
                query = "select count(id) from MediaMetadata where classtype=:classtype order by date desc"
        ),
        @NamedQuery(
                name = "countUserMedia",
                query = "select count(id) from MediaMetadata where classtype=:classtype and user_id=:userID order by date desc"
        ),
        @NamedQuery(
                name = "getAll",
                query = "from MediaMetadata where classtype=:classtype order by date desc"
        ),
        @NamedQuery(
                name = "getUserMedia",
                query = "from MediaMetadata where classtype=:classtype and user_id=:userID order by date desc"
        ),
        @NamedQuery(
                name = "deleteUserMedia",
                query = "delete from MediaMetadata where user_id=:userID"
        )
})
@Entity(name = "User")
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long userID;

    @NaturalId(mutable = true)
    private String login;

    @JsonIgnore
    private String password;

    @Column(name = "name")
    private String userName;

    private String description;

    @JsonIgnore
    @Column(updatable = false)
    private int enabled = 1;

    @JsonIgnore
    @OneToMany(targetEntity = Photo.class, mappedBy = "user")
    private List<Photo> photosList;

    @JsonIgnore
    @OneToMany(targetEntity = Video.class, mappedBy = "user")
    private List<Video> videosList;

    @ManyToOne(targetEntity = Role.class)
    private Role role;


    public User() {
    }


    public User(String login, String password, String userName, String description, Role role) {
        this.login = login;
        this.password = password;
        this.userName = userName;
        this.description = description;
        this.role = role;
    }


    @Override
    public String toString() {
        return "User ID: " + getUserID() + " Login: " + getLogin() + " User name: " + getUserName() +
                " Role: " + (getRole().getRoleName()) + " Description: " + getDescription();
    }


    public List<Video> getVideosList() {
        return videosList;
    }

    public void setVideosList(List<Video> videosList) {
        this.videosList = videosList;
    }


    public void setUserID(long userID) {
        this.userID = userID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUserID() {
        return userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public Role getRole() {
        return role;
    }

    public String getDescription() {
        return description;
    }

    public List<Photo> getPhotosList() {
        return photosList;
    }

    public void setPhotosList(List<Photo> photosList) {
        this.photosList = photosList;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
