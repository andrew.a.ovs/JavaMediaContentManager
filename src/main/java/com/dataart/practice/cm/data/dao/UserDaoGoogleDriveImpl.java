package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.data.model.Video;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by aovsyannikov on 8/18/2016.
 */
@Repository
@Profile("GoogleDrive")
public class UserDaoGoogleDriveImpl extends UserDaoImpl {

    private Drive driveService;

    @Autowired
    private PhotoDao photoDao;

    @Autowired
    private VideoDao videoDao;

    public UserDaoGoogleDriveImpl() {
    }

    @Autowired
    public UserDaoGoogleDriveImpl(HttpTransport httpTransport, JsonFactory jsonFactory) {
        try {
            GoogleCredential credential = GoogleCredential
                    .fromStream(this.getClass().getClassLoader().getResourceAsStream("javamcmgooglecredentials.json"))
                    .createScoped(Collections.singleton(DriveScopes.DRIVE));
            driveService = new Drive.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("JavaMediaContentManager")
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserByLogin(String login) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        return session.byNaturalId(User.class).using("login", login).load();
    }

    @Override
    public User getUserByID(long userID) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setPhotosList(photoDao.getUserMedia(user.getUserID()));
        user.setVideosList(videoDao.getUserMedia(user.getUserID()));
        return user;
    }

    @Override
    public void deleteUser(long userID) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        List<Photo> photoList = photoDao.getUserMedia(userID);
        for (Photo photo : photoList) {
            driveService
                    .files()
                    .delete(photo.getDriveID())
                    .execute();
        }
        List<Video> videoList = videoDao.getUserMedia(userID);
        for (Video video : videoList) {
            driveService
                    .files()
                    .delete(video.getDriveID())
                    .execute();
        }
        session
                .getNamedQuery("deleteUserMedia")
                .setParameter("userID", userID)
                .executeUpdate();
        session
                .getNamedQuery("deleteUser")
                .setParameter("userID", userID)
                .executeUpdate();
    }
}
