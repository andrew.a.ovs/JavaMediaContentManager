package com.dataart.practice.cm.rest.resource;

import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.service.UserService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by aovsyannikov on 9/23/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class UserMediaResourceImplTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    private User admin;

    private User user;

    private User anotherUser;

    @Value("${hostname}")
    private String hostname;

    @Value("${jsonContentType}")
    private String json;

    @Value("${originHeader}")
    private String origin;

    @Value("${contentDispositionHeader}")
    private String contentDisposition;

    @Value("${contentDispositionTemplate}")
    private String cdTemplate;

    @Value("${usersRoot}")
    private String users;

    @Value("${getUserPhotos}")
    private String testStringGetUserPhotos;

    @Value("${invalidPaginationParams}")
    private String testStringInvalidPagination;

    @Value("${getUserPhotosCount}")
    private String testStringGetUserPhotosCount;

    @Value("${getUserPhoto}")
    private String testStringGetUserPhoto;

    @Value("${unauthorizedPhotoRemovalAttempt}")
    private String testStringUnauthPhotoRemoval;

    @Value("${photoRemovalSuccess}")
    private String testStringPhotoRemovalSuccess;

    @Value("${getUserVideos}")
    private String testStringGetUserVideos;

    @Value("${getUserVideosCount}")
    private String testStringGetUserVideosCount;

    @Value("${getUserVideo}")
    private String testStringGetUserVideo;

    @Value("${unauthorizedVideoRemovalAttempt}")
    private String testStringUnauthVideoRemoval;

    @Value("${videoRemovalSuccess}")
    private String testStringVideoRemovalSuccess;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        admin = userService.getUserById(1L);
        user = userService.getUserById(2L);
        anotherUser = userService.getUserById(3L);
    }

    @Test
    public void test0GetUserPhotos() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/photos")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserPhotos)
        );
        assertTrue(mockMvc
                .perform(get(users + "/2/photos")
                        .header(origin, hostname)
                        .param("page", "1")
                        .param("pageSize", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserPhotos)
        );
        assertTrue(mockMvc
                .perform(get(users + "/2/photos")
                        .header(origin, hostname)
                        .param("page", "2")
                        .param("pageSize", "3"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringInvalidPagination)
        );
    }

    @Test
    public void test1GetUserPhotosCount() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/photos/count")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserPhotosCount)
        );
    }

    @Test
    public void test2GetUserPhoto() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/photos/2")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserPhoto)
        );
        mockMvc
                .perform(get(users + "/2/photos/666")
                        .header(origin, hostname))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(json));
    }

    @Test
    public void test3DeleteUserPhoto() throws Exception {
        assertTrue(mockMvc
                .perform(delete(users + "/2/photos/2")
                        .sessionAttr("user", anotherUser)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthPhotoRemoval)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/2/photos/2")
                        .sessionAttr("user", user)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPhotoRemovalSuccess)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/2/photos/3")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringPhotoRemovalSuccess)
        );
    }

    @Test
    public void test4DownloadPhoto() throws Exception {
        mockMvc
                .perform(get(users + "/2/photos/4/download")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType("image/jpeg"))
                .andExpect(header().string(contentDisposition, cdTemplate + "testPhoto2")
                );
    }

    @Test
    public void test5GetUserVideos() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/videos")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserVideos)
        );
        assertTrue(mockMvc
                .perform(get(users + "/2/videos")
                        .header(origin, hostname)
                        .param("page", "1")
                        .param("pageSize", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserVideos)
        );
        assertTrue(mockMvc
                .perform(get(users + "/2/videos")
                        .header(origin, hostname)
                        .param("page", "2")
                        .param("pageSize", "3"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringInvalidPagination)
        );
    }

    @Test
    public void test6GetUserVideosCount() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/videos/count")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserVideosCount)
        );
    }

    @Test
    public void test7GetUserVideo() throws Exception {
        assertTrue(mockMvc
                .perform(get(users + "/2/videos/2")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringGetUserVideo)
        );
        mockMvc
                .perform(get(users + "/2/videos/666")
                        .header(origin, hostname))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(json));
    }

    @Test
    public void test8DeleteUserVideo() throws Exception {
        assertTrue(mockMvc
                .perform(delete(users + "/2/videos/2")
                        .sessionAttr("user", anotherUser)
                        .header(origin, hostname))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringUnauthVideoRemoval)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/2/videos/2")
                        .sessionAttr("user", user)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringVideoRemovalSuccess)
        );
        assertTrue(mockMvc
                .perform(delete(users + "/2/videos/3")
                        .sessionAttr("user", admin)
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType(json))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .equals(testStringVideoRemovalSuccess)
        );
    }

    @Test
    public void test9DownloadVideo() throws Exception {
        mockMvc
                .perform(get(users + "/2/videos/4/download")
                        .header(origin, hostname))
                .andExpect(status().isOk())
                .andExpect(content().contentType("video/mp4"))
                .andExpect(header().string(contentDisposition, cdTemplate + "testVideo2")
                );
    }
}
