package com.dataart.practice.cm.rest.resource.impl;

import com.dataart.practice.cm.data.model.Role;
import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.rest.resource.BaseResource;
import com.dataart.practice.cm.rest.resource.UsersResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UsersResourceImpl extends BaseResource implements UsersResource {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Cacheable("users")
    public ResponseEntity<List<Resource>> getUsers(
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                    int pageSize,
            HttpSession session) {
        LinkedList<Resource> resourceList = new LinkedList<>();
        HttpStatus status;
        if ("ADMIN".equals(((User) session.getAttribute("user")).getRole().getRoleName())) {
            try {
                for (User user : userService.getAllUsers()) {
                    resourceList.addLast(turnUserToResource(user));
                }
                return performPagination(resourceList, page, pageSize);
            } catch (Exception e) {
                resourceList.add(new Resource<>(e.getMessage()));
                status = HttpStatus.NOT_FOUND;
            }
        } else {
            resourceList.add(new Resource<>("Only admin can see users list"));
            status = HttpStatus.FORBIDDEN;
        }
        return new ResponseEntity<>(resourceList, status);
    }

    public ResponseEntity<Resource> getUsersCount() {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        try {
            resource = new Resource<>(userService.getAllUsers().size(),
                    linkTo(UsersResourceImpl.class).withRel("users"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"users"}, allEntries = true)
    public ResponseEntity<Resource> registerUser(@RequestParam(name = "login")
                                                         String login,
                                                 @RequestParam(name = "password")
                                                         String password,
                                                 @RequestParam(name = "confirmPassword")
                                                         String confirmPassword,
                                                 @RequestParam(name = "name", required = false)
                                                         String name,
                                                 @RequestParam(name = "description", required = false)
                                                         String description) {
        String responseMessage = "User creation failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Resource resource = new Resource<>(responseMessage);
        try {
            if (!StringUtils.isEmpty(login)
                    && !StringUtils.isEmpty(password)
                    && password.equals(confirmPassword)
                    && !userService.isLoginTaken(login)) {
                String hashPassword = passwordEncoder.encode(password);
                User user = new User(login, hashPassword, name, description, new Role(0, "USER"));
                userService.registerUser(user);
                resource = turnUserToResource(user);
                status = HttpStatus.CREATED;
            } else if (StringUtils.isEmpty(login)) {
                responseMessage = "Registration failed due to empty login parameter" + login;
                status = HttpStatus.BAD_REQUEST;
                resource = new Resource<>(responseMessage);
            } else if (StringUtils.isEmpty(password) || !password.equals(confirmPassword)) {
                responseMessage = "Registration failed due to empty or mismatching password parameter";
                status = HttpStatus.BAD_REQUEST;
                resource = new Resource<>(responseMessage);
            } else if (userService.isLoginTaken(login)) {
                responseMessage = "Registration failed due to already taken login";
                status = HttpStatus.BAD_REQUEST;
                resource = new Resource<>(responseMessage);
            }
        } catch (Exception e) {
            responseMessage += e.getMessage();
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    public ResponseEntity<Resource> addUser(@RequestParam(name = "login")
                                                    String login,
                                            @RequestParam(name = "password")
                                                    String password,
                                            @RequestParam(name = "confirmPassword")
                                                    String confirmPassword,
                                            @RequestParam(name = "name", required = false)
                                                    String name,
                                            @RequestParam(name = "description", required = false)
                                                    String description,
                                            HttpSession session) {
        if ("ADMIN".equals(((User) session.getAttribute("user")).getRole().getRoleName())) {
            return registerUser(login, password, confirmPassword, name, description);
        } else {
            return new ResponseEntity<>(new Resource<>("Only administrator can add new users"), HttpStatus.FORBIDDEN);
        }
    }
}
