package com.dataart.practice.cm.rest.resource;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping(value = "/mcm/api/v1/users/{userID}")
public interface UserMediaResource {

    @RequestMapping(value = "photos", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Resource>> getUserPhotos(@PathVariable(value = "userID")
                                                         long userID,
                                                 @RequestParam(value = "page", required = false, defaultValue = "0")
                                                         int page,
                                                 @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                                                         int pageSize);

    @RequestMapping(value = "photos", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> uploadPhoto(@PathVariable(value = "userID") long userID,
                                         @RequestParam(value = "file") MultipartFile file,
                                         HttpSession session);


    @RequestMapping(value = "photos/count", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserPhotosCount(@PathVariable(value = "userID") long userID);

    @RequestMapping(value = "photos/{photoID}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getPhoto(@PathVariable(value = "photoID") long photoID,
                                      @PathVariable(value = "userID") long userID);

    @RequestMapping(value = "photos/{photoID}", method = RequestMethod.DELETE, produces = "application/json")
    ResponseEntity<Resource> deleteUserPhotoById(@PathVariable(value = "photoID") long photoID,
                                                 HttpSession session);

    @RequestMapping(value = "photos/{photoID}/download", method = RequestMethod.GET)
    void downloadUserPhotoById(@PathVariable("photoID") long photoID, HttpServletResponse resp);

    @RequestMapping(value = "videos", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Resource>> getUserVideos(@PathVariable(value = "userID")
                                                         long userID,
                                                 @RequestParam(value = "page", required = false, defaultValue = "0")
                                                         int page,
                                                 @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                                                         int pageSize);

    @RequestMapping(value = "videos", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<Resource> uploadVideo(@PathVariable(value = "userID") long userID,
                                         @RequestParam(value = "file") MultipartFile file,
                                         HttpSession session);

    @RequestMapping(value = "videos/count", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserVideosCount(@PathVariable(value = "userID") long userID);

    @RequestMapping(value = "videos/{videoID}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getUserVideo(@PathVariable(value = "videoID") long videoID,
                                          @PathVariable(value = "userID") long userID);

    @RequestMapping(value = "videos/{videoID}", method = RequestMethod.DELETE, produces = "application/json")
    ResponseEntity<Resource> deleteUserVideoById(@PathVariable(value = "videoID") long videoID,
                                                 HttpSession session);

    @RequestMapping(value = "videos/{videoID}/download", method = RequestMethod.GET)
    void downloadUserVideoById(@PathVariable("videoID") long videoID, HttpServletResponse resp);
}
