package com.dataart.practice.cm.rest.resource.impl;

import com.dataart.practice.cm.data.model.User;
import com.dataart.practice.cm.rest.resource.BaseResource;
import com.dataart.practice.cm.rest.resource.UserProfileResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserProfileResourceImpl extends BaseResource implements UserProfileResource {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Cacheable("users")
    public ResponseEntity<Resource> getUser(@PathVariable(value = "userID") long userID) {
        String responseMessage = "Request failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Resource resource;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        try {
            User user = userService.getUserById(userID);
            resource = turnUserToResource(user);
            status = HttpStatus.OK;
        } catch (Exception e) {
            responseMessage += e.getMessage();
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"photos", "userPhotos", "videos", "userVideos", "users"}, key = "#userID", allEntries = true)
    public ResponseEntity<Resource> deleteUser(@PathVariable(value = "userID") long userID,
                                               HttpSession session) {
        String message = "Request failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            if (((User) session.getAttribute("user")).getRole().getRoleName().equals("ADMIN")
                    && (((User) session.getAttribute("user")).getUserID() != userID)) {
                userService.deleteUser(userID);
                message = "User with ID: " + userID + " was successfully deleted";
                status = HttpStatus.OK;
            } else {
                message = "You can delete users only if you are an admin. And you cannot delete yourself.";
                status = HttpStatus.FORBIDDEN;
            }
        } catch (Exception e) {
            message += e.getMessage();
        }
        return new ResponseEntity<>(new Resource<>(message), status);
    }

    @Cacheable("users")
    public ResponseEntity<Resource> getUserLogin(@PathVariable(value = "userID") long userID) {
        Resource<String> resource = new Resource<>("Internal error occurred: ");
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        try {
            String login = userService.getUserById(userID).getLogin();
            resource = new Resource<>(
                    login,
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("login")
                            .withSelfRel(),
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .withRel("user"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"users"}, key = "#userID", allEntries = true)
    public ResponseEntity<Resource> changeUserLogin(@RequestParam(value = "newLogin") String newLogin,
                                                    @PathVariable(value = "userID") long userID,
                                                    HttpSession session) {
        String responseMessage = "Login change failed due to exception: ";
        HttpStatus status = HttpStatus.CREATED;
        Resource<String> resource = new Resource<>(responseMessage);
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                if (!StringUtils.isEmpty(newLogin) && newLogin.length() <= 50 && !userService.isLoginTaken(newLogin)) {
                    userService.setNewLogin(userID, newLogin);
                    currentUser.setLogin(newLogin);
                    responseMessage = "Login was successfully changed to: " + newLogin;
                    resource = new Resource<>(currentUser.getLogin());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("login")
                                    .withSelfRel());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .withRel("user"));
                } else if (StringUtils.isEmpty(newLogin) || !(newLogin.length() <= 50)) {
                    responseMessage = "Failed due to invalid login: login cannot be empty or exceed 50 characters";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else if (userService.isLoginTaken(newLogin)) {
                    responseMessage = "Failed due to already taken login.";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                resource = new Resource<>(responseMessage);
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        } else {
            responseMessage = "Only user himself can change his login";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    @Cacheable("users")
    public ResponseEntity<Resource> getUserName(@PathVariable(value = "userID") long userID) {
        Resource<String> resource = new Resource<>("Internal error occurred: ");
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        try {
            String name = userService.getUserById(userID).getUserName();
            resource = new Resource<>(
                    name,
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("name")
                            .withSelfRel(),
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .withRel("user"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"users"}, key = "#userID", allEntries = true)
    public ResponseEntity<Resource> changeUserName(@RequestParam(value = "newName") String newName,
                                                   @PathVariable(value = "userID") long userID,
                                                   HttpSession session) {
        String responseMessage = "Username change failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Resource<String> resource;
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                if (newName.length() <= 50) {
                    userService.setNewUsername(userID, newName);
                    currentUser.setUserName(newName);
                    responseMessage = "Username was successfully changed to: " + newName;
                    status = HttpStatus.CREATED;
                    resource = new Resource<>(currentUser.getUserName());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("username")
                                    .withSelfRel());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .withRel("user"));
                } else {
                    responseMessage = "Failed due to invalid username: username cannot exceed 50 characters";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                resource = new Resource<>(responseMessage);
            }
        } else {
            responseMessage = "Only user himself can change his username";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    @Cacheable("users")
    public ResponseEntity<Resource> getUserDescription(@PathVariable(value = "userID") long userID) {
        Resource<String> resource = new Resource<>("Internal error occurred: ");
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        try {
            String description = userService.getUserById(userID).getDescription();
            resource = new Resource<>(
                    description,
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .slash("login")
                            .withSelfRel(),
                    linkTo(UsersResourceImpl.class)
                            .slash(userID)
                            .withRel("user"));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"users"}, key = "#userID", allEntries = true)
    public ResponseEntity<Resource> changeUserDescription(@RequestParam(value = "newDescription")
                                                                  String newDescription,
                                                          @PathVariable(value = "userID")
                                                                  long userID,
                                                          HttpSession session) {
        String responseMessage = "Description change failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Resource<String> resource;
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                if (newDescription.length() <= 255) {
                    userService.setNewDescription(userID, newDescription);
                    currentUser.setDescription(newDescription);
                    responseMessage = "Description was successfully changed to: " + newDescription;
                    status = HttpStatus.CREATED;
                    resource = new Resource<>(currentUser.getDescription());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("description")
                                    .withSelfRel());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .withRel("user"));
                } else {
                    responseMessage = "Failed due to invalid description: description cannot exceed 255 characters";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                resource = new Resource<>(responseMessage);
            }
        } else {
            responseMessage = "Only user himself can change his description";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }

    public ResponseEntity<Resource> changeUserPassword(@RequestParam(value = "newPassword")
                                                               String password,
                                                       @RequestParam(value = "confirmPassword")
                                                               String confirmPassword,
                                                       @PathVariable(value = "userID")
                                                               long userID,
                                                       HttpSession session) {
        String responseMessage = "Password change failed due to exception: ";
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Resource<String> resource;
        User currentUser = (User) session.getAttribute("user");
        if (currentUser.getUserID() == userID) {
            try {
                if (StringUtils.isEmpty(password) ||
                        StringUtils.isEmpty(confirmPassword) ||
                        !password.equals(confirmPassword)) {
                    responseMessage = "Invalid or not matching new password";
                    status = HttpStatus.BAD_REQUEST;
                    resource = new Resource<>(responseMessage);
                } else {
                    String encrPassword = passwordEncoder.encode(password);
                    userService.setNewPassword(userID, encrPassword);
                    currentUser.setPassword(encrPassword);
                    responseMessage = "Password change succeeded";
                    status = HttpStatus.CREATED;
                    resource = new Resource<>(currentUser.getDescription());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .slash("password")
                                    .withSelfRel());
                    resource
                            .add(linkTo(UsersResourceImpl.class)
                                    .slash(userID)
                                    .withRel("user"));
                }
            } catch (Exception e) {
                responseMessage += e.getMessage();
                resource = new Resource<>(responseMessage);
            }
        } else {
            responseMessage = "Only user himself can change his password";
            status = HttpStatus.FORBIDDEN;
            resource = new Resource<>(responseMessage);
        }
        return new ResponseEntity<>(resource, status);
    }
}
