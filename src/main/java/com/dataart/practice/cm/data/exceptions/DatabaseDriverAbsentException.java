package com.dataart.practice.cm.data.exceptions;

public class DatabaseDriverAbsentException extends ClassNotFoundException {
    public DatabaseDriverAbsentException(String s, Throwable ex) {
        super(s, ex);
    }
}
