package com.dataart.practice.cm.rest.resource;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping(value = "/mcm/api/v1/videos")
public interface VideosResource {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Resource>> getVideos(@RequestParam(value = "page", required = false, defaultValue = "0")
                                                     int page,
                                             @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                                                     int pageSize);

    @RequestMapping(value = "count", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getVideosCount();

    @RequestMapping(value = "{videoID}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getVideoById(@PathVariable(value = "videoID") long videoID);

    @RequestMapping(value = "{videoID}", method = RequestMethod.DELETE, produces = "application/json")
    ResponseEntity<Resource> deleteVideoById(@PathVariable(value = "videoID") long videoID, HttpSession session);

    @RequestMapping(value = "{videoID}/download", method = RequestMethod.GET)
    void downloadVideoById(@PathVariable("videoID") long videoID, HttpServletResponse resp);
}
