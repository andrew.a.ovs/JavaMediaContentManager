package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.Photo;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public interface PhotoDao extends GenericDao<Photo, Long> {
}
