package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.MediaMetadata;
import com.dataart.practice.cm.data.model.Photo;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("GoogleDrive")
public class PhotoDaoDriveImpl extends GenericDaoDriveImpl<Photo, Long> implements PhotoDao {

    public PhotoDaoDriveImpl(HttpTransport httpTransport, JsonFactory jsonFactory) {
        super(httpTransport, jsonFactory);
    }

    @Override
    public Photo getById(Long mediaId) throws Exception {
        Photo photo = new Photo(sessionFactory.getCurrentSession().load(MediaMetadata.class, mediaId));
        photo.setImageBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                .files()
                .get(photo.getDriveID())
                .executeMediaAsInputStream()));
        return photo;
    }

    @Override
    public void upload(Photo photo) throws Exception {
        photo.setPhotoID(generatePhotoId(photo));
        ByteArrayContent byteContent = new ByteArrayContent("image/" + photo.getExtension(), photo.getImageBytes());
        File file = new File().setMimeType("image/" + photo.getExtension());
        Drive.Files.Create create = driveService.files().create(file, byteContent);
        MediaHttpUploader uploader = create.getMediaHttpUploader();
        uploader.setDirectUploadEnabled(true);
        String driveID = create.execute().getId();
        photo.setDriveID(driveID);
        sessionFactory.getCurrentSession().save(MediaMetadata
                .getMetadataBuilder()
                .mediaId(photo.getPhotoID())
                .driveId(driveID)
                .user(photo.getUser())
                .filename(photo.getFileName())
                .date(photo.getDate())
                .size(photo.getSize())
                .extension(photo.getExtension())
                .type(photo.getClass().toString())
                .build());
    }

    private long generatePhotoId(Photo photo) {
        long sum = photo.getDate() + photo.getUser().getUserID();
        long diff = photo.getDate() - photo.getUser().getUserID();
        long uniqueID = sum + diff + photo.getSize() % ID_GENERATION_1
                - System.currentTimeMillis() % ID_GENERATION_2;
        return Long.parseLong(photo.getUser().getUserID() + String.valueOf(uniqueID).substring(7));
    }

    @Override
    public List<Photo> getAll() throws Exception {
        List<Photo> resultList = new ArrayList<>();
        List<MediaMetadata> metaList = sessionFactory
                .getCurrentSession()
                .getNamedQuery("getAll")
                .setParameter("classtype", entityClass.toString())
                .list();
        for (MediaMetadata meta : metaList) {
            Photo photo = new Photo(meta);
            photo.setImageBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                    .files()
                    .get(meta.getDriveID())
                    .executeMediaAsInputStream()));
            resultList.add(photo);
        }
        return resultList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<Photo> getUserMedia(Long userId) throws Exception {
        List<Photo> resultList = new ArrayList<>();
        List<MediaMetadata> metaList = sessionFactory
                .getCurrentSession()
                .getNamedQuery("getUserMedia")
                .setParameter("classtype", entityClass.toString())
                .setParameter("userID", userId)
                .list();
        for (MediaMetadata meta : metaList) {
            Photo photo = new Photo(meta);
            photo.setImageBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                    .files()
                    .get(meta.getDriveID())
                    .executeMediaAsInputStream()));
            resultList.add(photo);
        }
        return resultList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
