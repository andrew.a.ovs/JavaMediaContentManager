package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.exceptions.DatabaseDriverAbsentException;
import com.dataart.practice.cm.data.model.User;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by aovsyannikov on 8/4/2016.
 */
public interface UserDao {

    void registerUser(User user) throws DatabaseDriverAbsentException, SQLException, IOException;

    boolean isLoginTaken(String login) throws DatabaseDriverAbsentException, SQLException, IOException;

    User getUserByLogin(String login) throws Exception;

    User getUserByID(long userID) throws Exception;

    void deleteUser(long userID) throws Exception;

    List<User> getAllUsers() throws DatabaseDriverAbsentException, SQLException, IOException;

    void setNewLogin(long userID, String newLogin) throws DatabaseDriverAbsentException, SQLException,
            IOException;

    void setNewUsername(long userID, String newUsername) throws DatabaseDriverAbsentException, SQLException,
            IOException;

    void setNewPassword(long userID, String newPassword) throws NoSuchAlgorithmException,
            DatabaseDriverAbsentException, SQLException, IOException;

    void setNewDescription(long userID, String newDescription) throws DatabaseDriverAbsentException, SQLException,
            IOException;
}
