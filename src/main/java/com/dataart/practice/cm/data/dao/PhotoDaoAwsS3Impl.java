package com.dataart.practice.cm.data.dao;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import com.dataart.practice.cm.data.model.Photo;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("AWSS3")
public class PhotoDaoAwsS3Impl extends GenericDaoAwsS3Impl<Photo, Long> implements PhotoDao {

    @Override
    public Photo getById(Long photoID) throws Exception {
        S3Object s3Object = amazonS3.getObject(bucketName, String.valueOf(photoID));
        ObjectMetadata metadata = s3Object.getObjectMetadata();
        return new Photo(photoID,
                IOUtils.toByteArray(s3Object.getObjectContent()),
                userDao.getUserByID(Long.parseLong(metadata.getUserMetaDataOf("userID"))),
                metadata.getUserMetaDataOf("filename"),
                Long.parseLong(metadata.getUserMetaDataOf("date")),
                (int) metadata.getContentLength(),
                metadata.getUserMetaDataOf("extension"));
    }

    @Override
    public void upload(Photo photo) {
        InputStream imageStream = new ByteArrayInputStream(photo.getImageBytes());
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.addUserMetadata("userID", String.valueOf(photo.getUser().getUserID()));
        objectMetadata.addUserMetadata("filename", photo.getFileName());
        objectMetadata.addUserMetadata("date", String.valueOf(photo.getDate()));
        objectMetadata.addUserMetadata("extension", photo.getExtension());
        objectMetadata.setContentType("image/" + photo.getExtension());
        String key = generatePhotoId(photo);
        photo.setPhotoID(Long.parseLong(key));
        amazonS3.putObject(bucketName, key, imageStream, objectMetadata);
    }

    private String generatePhotoId(Photo photo) {
        long sum = photo.getDate() + photo.getUser().getUserID();
        long diff = photo.getDate() - photo.getUser().getUserID();
        long uniqueID = sum + diff + photo.getSize() % ID_GENERATION_1
                - System.currentTimeMillis() % ID_GENERATION_2;
        return photo.getUser().getUserID() + String.valueOf(uniqueID).substring(7);
    }

    @Override
    public long countAll() throws Exception {
        return getAll().size();
    }

    @Override
    public List<Photo> getAll() throws Exception {
        List<Photo> photosList = new ArrayList<>();
        for (S3ObjectSummary sum : amazonS3.listObjects(bucketName).getObjectSummaries()) {
            S3Object obj = amazonS3.getObject(bucketName, sum.getKey());
            if (obj
                    .getObjectMetadata()
                    .getContentType()
                    .contains("image")) {
                photosList.add(getById(Long.parseLong(sum.getKey())));
                obj.close();
            }
        }
        return photosList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public long countUserMedia(Long userId) throws Exception {
        return getUserMedia(userId).size();
    }

    @Override
    public List<Photo> getUserMedia(Long userId) throws Exception {
        List<Photo> photosList = new ArrayList<>();
        for (S3ObjectSummary sum : amazonS3.listObjects(bucketName).getObjectSummaries()) {
            S3Object obj = amazonS3.getObject(bucketName, sum.getKey());
            if (obj
                    .getObjectMetadata()
                    .getContentType()
                    .contains("image")
                    && (Long.parseLong(obj.getObjectMetadata().getUserMetaDataOf("userID")) == userId)) {
                photosList.add(getById(Long.parseLong(sum.getKey())));
                obj.close();
            }
        }
        return photosList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
