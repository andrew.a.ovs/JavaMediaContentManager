package com.dataart.practice.cm.rest.resource;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by aovsyannikov on 9/22/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginLogoutTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Value("${hostname}")
    private String hostname;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void test0Login() throws Exception {
        mockMvc
                .perform(post("/mcm/api/v1/login")
                        .param("login", "admin")
                        .param("password", "admin")
                        .header("Origin", hostname))
                .andExpect(authenticated())
                .andExpect(status().isOk());
        mockMvc
                .perform(post("/mcm/api/v1/login")
                        .param("login", "admin")
                        .param("password", "admi")
                        .header("Origin", hostname))
                .andExpect(unauthenticated())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test1Logout() throws Exception {
        mockMvc
                .perform(post("/mcm/api/v1/logout")
                        .header("Origin", hostname))
                .andExpect(status().isOk());
    }


}
