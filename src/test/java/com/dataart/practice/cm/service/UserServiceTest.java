package com.dataart.practice.cm.service;

import com.dataart.practice.cm.data.model.Role;
import com.dataart.practice.cm.data.model.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by aovsyannikov on 9/20/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void test0GetAllUsers() throws Exception {
        List<User> queriedList = userService.getAllUsers();
        assertTrue(queriedList
                .stream()
                .filter(item -> item instanceof User)
                .count() == 5
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getLogin().equals("admin"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getLogin().equals("testLogin1"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getLogin().equals("testLogin2"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getLogin().equals("testLogin3"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getLogin().equals("testLogin4"))
        );
    }

    @Test
    public void testGetUserByLogin() throws Exception {
        User user = userService.getUserByLogin("admin");
        assertNotNull(user);
        assertNotNull(user.getPhotosList());
        assertFalse(user.getPhotosList().isEmpty());
        assertNotNull(user.getVideosList());
        assertFalse(user.getVideosList().isEmpty());
        assertEquals(user.getLogin(), "admin");
        assertEquals(user.getUserName(), "John Doe");
        assertEquals(user.getDescription(), "default administrator account");
    }

    @Test
    public void testGetUserById() throws Exception {
        User user = userService.getUserById(1);
        assertNotNull(user);
        assertNotNull(user.getPhotosList());
        assertFalse(user.getPhotosList().isEmpty());
        assertNotNull(user.getVideosList());
        assertFalse(user.getVideosList().isEmpty());
        assertEquals(user.getLogin(), "admin");
        assertEquals(user.getUserName(), "John Doe");
        assertEquals(user.getDescription(), "default administrator account");
    }

    @Test
    public void testIsLoginTaken() throws Exception {
        assertTrue(userService.isLoginTaken("admin"));
        assertFalse(userService.isLoginTaken("Admin"));
        assertFalse(userService.isLoginTaken("thisLoginIsDefinitelyNotTaken"));
    }

    @Test
    public void testRegisterUser() throws Exception {
        User user = new User("testRegistration",
                "c0145bae4e401c784bb3e52f7810ea8e925bbbbb7dd248aedce2c8d51415b17469031edd4fd87e2b",
                "username",
                "description",
                new Role(0, "USER"));
        userService.registerUser(user);
        User queriedUser = userService.getUserByLogin(user.getLogin());
        assertNotNull(queriedUser);
        user.setUserID(queriedUser.getUserID());
        assertTrue(user.toString().equals(queriedUser.toString()));
    }

    @Test
    public void testDeleteUser() throws Exception {
        try {
            userService.deleteUser(2);
            userService.getUserById(2);
            throw new AssertionError("testDeleteUser failed");
        } catch (org.hibernate.ObjectNotFoundException onf) {
        }
    }

    @Test
    public void testSetNewLogin() throws Exception {
        userService.setNewLogin(3, "newTestLogin");
        assertNotNull(userService.getUserByLogin("newTestLogin"));
    }

    @Test
    public void testSetNewUsername() throws Exception {
        userService.setNewUsername(4, "newTestUsername");
        assertEquals(userService.getUserById(4).getUserName(), "newTestUsername");
    }

    @Test
    public void testSetNewDescription() throws Exception {
        userService.setNewDescription(5, "newTestDescription");
        assertEquals(userService.getUserById(5).getDescription(), "newTestDescription");
    }

    @Test
    public void testSetNewPassword() throws Exception {
        userService.setNewPassword(5, "newTestPassword");
        assertEquals(userService.getUserById(5).getPassword(), "newTestPassword");
    }
}
