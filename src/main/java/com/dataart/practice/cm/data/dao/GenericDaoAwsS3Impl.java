package com.dataart.practice.cm.data.dao;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
public abstract class GenericDaoAwsS3Impl<T, ID extends Serializable> implements GenericDao<T, ID> {

    @Autowired
    protected AmazonS3 amazonS3;

    @Autowired
    protected UserDao userDao;

    protected final int ID_GENERATION_1 = 10;

    protected final int ID_GENERATION_2 = 1000000000;

    @Value("${aws-bucketName}")
    protected String bucketName;

    public GenericDaoAwsS3Impl() {
    }

    @Autowired
    public GenericDaoAwsS3Impl(AmazonS3 amazonS3, UserDao userDao) {
        this.amazonS3 = amazonS3;
        this.amazonS3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_CENTRAL_1));
        this.userDao = userDao;
    }

    @Override
    public void delete(ID mediaId) {
        amazonS3.deleteObject(bucketName, String.valueOf(mediaId));
    }
}
