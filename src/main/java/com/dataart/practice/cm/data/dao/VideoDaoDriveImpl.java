package com.dataart.practice.cm.data.dao;

import com.dataart.practice.cm.data.model.MediaMetadata;
import com.dataart.practice.cm.data.model.Video;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 10/5/2016.
 */
@Repository
@Profile("GoogleDrive")
public class VideoDaoDriveImpl extends GenericDaoDriveImpl<Video, Long> implements VideoDao {

    public VideoDaoDriveImpl(HttpTransport httpTransport, JsonFactory jsonFactory) {
        super(httpTransport, jsonFactory);
    }

    @Override
    public Video getById(Long mediaId) throws Exception {
        Video video = new Video(sessionFactory.getCurrentSession().load(MediaMetadata.class, mediaId));
        video.setVideoBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                .files()
                .get(video.getDriveID())
                .executeMediaAsInputStream()));
        return video;
    }

    @Override
    public void upload(Video video) throws Exception {
        video.setVideoID(generateVideoId(video));
        ByteArrayContent byteContent = new ByteArrayContent("video/" + video.getExtension(), video.getVideoBytes());
        File file = new File().setMimeType("video/" + video.getExtension());
        Drive.Files.Create create = driveService.files().create(file, byteContent);
        MediaHttpUploader uploader = create.getMediaHttpUploader();
        uploader.setDirectUploadEnabled(true);
        String driveID = create.execute().getId();
        video.setDriveID(driveID);
        sessionFactory.getCurrentSession().save(MediaMetadata
                .getMetadataBuilder()
                .mediaId(video.getVideoID())
                .driveId(driveID)
                .user(video.getUser())
                .filename(video.getFileName())
                .date(video.getDate())
                .size(video.getSize())
                .extension(video.getExtension())
                .type(video.getClass().toString())
                .build());
    }

    private long generateVideoId(Video video) {
        long sum = video.getDate() + video.getUser().getUserID();
        long diff = video.getDate() - video.getUser().getUserID();
        long uniqueID = sum + diff + video.getSize() % ID_GENERATION_1
                - System.currentTimeMillis() % ID_GENERATION_2;
        return Long.parseLong(video.getUser().getUserID() + String.valueOf(uniqueID).substring(7));
    }

    @Override
    public List<Video> getAll() throws Exception {
        List<Video> resultList = new ArrayList<>();
        List<MediaMetadata> metaList = sessionFactory
                .getCurrentSession()
                .getNamedQuery("getAll")
                .setParameter("classtype", entityClass.toString())
                .list();
        for (MediaMetadata meta : metaList) {
            Video video = new Video(meta);
            video.setVideoBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                    .files()
                    .get(meta.getDriveID())
                    .executeMediaAsInputStream()));
            resultList.add(video);
        }
        return resultList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<Video> getUserMedia(Long userId) throws Exception {
        List<Video> resultList = new ArrayList<>();
        List<MediaMetadata> metaList = sessionFactory
                .getCurrentSession()
                .getNamedQuery("getUserMedia")
                .setParameter("classtype", entityClass.toString())
                .setParameter("userID", userId)
                .list();
        for (MediaMetadata meta : metaList) {
            Video video = new Video(meta);
            video.setVideoBytes(com.amazonaws.util.IOUtils.toByteArray(driveService
                    .files()
                    .get(meta.getDriveID())
                    .executeMediaAsInputStream()));
            resultList.add(video);
        }
        return resultList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
