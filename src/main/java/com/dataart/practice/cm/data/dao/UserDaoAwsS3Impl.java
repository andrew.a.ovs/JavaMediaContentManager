package com.dataart.practice.cm.data.dao;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.dataart.practice.cm.data.exceptions.DatabaseDriverAbsentException;
import com.dataart.practice.cm.data.model.User;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.stream.Collectors;

/**
 * Created by aovsyannikov on 8/4/2016.
 */
@Repository
@Profile("AWSS3")
@PropertySource("classpath:aws.properties")
public class UserDaoAwsS3Impl extends UserDaoImpl {

    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private PhotoDao photoDao;

    @Autowired
    private VideoDao videoDao;

    @Value("${aws-bucketName}")
    private String bucketName;

    @Override
    public User getUserByLogin(String login) throws DatabaseDriverAbsentException, SQLException, IOException {
        Session session = sessionFactory.getCurrentSession();
        return session.byNaturalId(User.class).using("login", login).load();
    }

    @Override
    public User getUserByID(long userID) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, userID);
        user.setPhotosList(photoDao.getUserMedia(userID)
                .stream()
                .sorted()
                .collect(Collectors.toList()));
        user.setVideosList(videoDao.getUserMedia(userID)
                .stream()
                .sorted()
                .collect(Collectors.toList()));
        return user;
    }

    @Override
    public void deleteUser(long userID) throws DatabaseDriverAbsentException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        amazonS3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_CENTRAL_1));
        for (S3ObjectSummary sum : amazonS3
                .listObjects(bucketName, String.valueOf(userID))
                .getObjectSummaries()) {
            amazonS3.deleteObject(bucketName, sum.getKey());
        }
        session
                .getNamedQuery("deleteUser")
                .setParameter("userID", userID)
                .executeUpdate();
    }
}
