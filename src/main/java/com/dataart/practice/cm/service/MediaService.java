package com.dataart.practice.cm.service;

import com.dataart.practice.cm.data.dao.PhotoDao;
import com.dataart.practice.cm.data.dao.VideoDao;
import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by aovsyannikov on 8/3/2016.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MediaService {

    @Autowired
    private PhotoDao photoDao;

    @Autowired
    private VideoDao videoDao;

    public Photo getPhotoById(long photoID) throws Exception {
        return photoDao.getById(photoID);
    }

    public Video getVideoById(long videoID) throws Exception {
        return videoDao.getById(videoID);
    }


    public void deletePhoto(long photoID) throws Exception {
        photoDao.delete(photoID);
    }


    public void deleteVideo(long videoID) throws Exception {
        videoDao.delete(videoID);
    }


    public void uploadPhoto(Photo photo) throws Exception {
        photoDao.upload(photo);
    }


    public void uploadVideo(Video video) throws Exception {
        videoDao.upload(video);
    }

    public long countAllPhotos() throws Exception {
        return photoDao.countAll();
    }

    public List<Photo> getAllPhotos() throws Exception {
        return photoDao.getAll();
    }

    public long countAllVideos() throws Exception {
        return videoDao.countAll();
    }

    public List<Video> getAllVideos() throws Exception {
        return videoDao.getAll();
    }

    public long countUserPhotos(long userID) throws Exception {
        return photoDao.countUserMedia(userID);
    }

    public List<Photo> getUserPhotos(long userID) throws Exception {
        return photoDao.getUserMedia(userID);
    }

    public long countUserVideos(long userID) throws Exception {
        return videoDao.countUserMedia(userID);
    }

    public List<Video> getUserVideos(long userID) throws Exception {
        return videoDao.getUserMedia(userID);
    }
}
