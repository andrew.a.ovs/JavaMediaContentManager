package com.dataart.practice.cm.rest.resource;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping(value = "/mcm/api/v1/photos")
public interface PhotosResource {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Resource>> getPhotos(@RequestParam(value = "page", required = false, defaultValue = "0")
                                                     int page,
                                             @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                                                     int pageSize);

    @RequestMapping(value = "count", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getPhotosCount();

    @RequestMapping(value = "{photoID}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<Resource> getPhoto(@PathVariable(value = "photoID") long photoID);

    @RequestMapping(value = "{photoID}", method = RequestMethod.DELETE, produces = "application/json")
    ResponseEntity<Resource> deletePhotoById(@PathVariable(value = "photoID") long photoID, HttpSession session);

    @RequestMapping(value = "{photoID}/download", method = RequestMethod.GET)
    void downloadPhotoById(@PathVariable("photoID") long photoID, HttpServletResponse resp);
}
