package com.dataart.practice.cm.service;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.data.model.Video;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by aovsyannikov on 9/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-config.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class MediaServiceTest {

    @Autowired
    private MediaService mediaService;

    @Test
    public void test00GetAllPhotos() throws Exception {
        List<Photo> queriedList = mediaService.getAllPhotos();
        assertTrue(queriedList
                .stream()
                .filter(item -> item instanceof Photo)
                .count() == 7
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testPhoto1"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testPhoto2"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testPhoto3"))
        );
    }

    @Test
    public void test0GetUserPhotos() throws Exception {
        List<Photo> queriedList = mediaService.getUserPhotos(2);
        assertTrue(queriedList
                .stream()
                .filter(item -> item instanceof Photo)
                .count() == 3
        );
        assertTrue(queriedList
                .stream()
                .allMatch(item -> item.getFileName().equals("testPhoto2"))
        );
    }

    @Test
    public void test11GetPhotosCount() throws Exception {
        assertTrue(mediaService.countAllPhotos() == 7);
    }

    @Test
    public void test1GetUserPhotosCount() throws Exception {
        assertTrue(mediaService.countUserPhotos(2) == 3);
    }

    @Test
    public void test22GetAllVideos() throws Exception {
        List<Video> queriedList = mediaService.getAllVideos();
        assertTrue(queriedList
                .stream()
                .filter(item -> item instanceof Video)
                .count() == 7
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testVideo1"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testVideo2"))
        );
        assertTrue(queriedList
                .stream()
                .anyMatch(item -> item.getFileName().equals("testVideo3"))
        );
    }

    @Test
    public void test2GetUserVideos() throws Exception {
        List<Video> queriedList = mediaService.getUserVideos(2);
        assertTrue(queriedList
                .stream()
                .filter(item -> item instanceof Video)
                .count() == 3
        );
        assertTrue(queriedList
                .stream()
                .allMatch(item -> item.getFileName().equals("testVideo2"))
        );
    }

    @Test
    public void test33GetVideosCount() throws Exception {
        assertTrue(mediaService.countAllVideos() == 7);
    }

    @Test
    public void test3GetUserVideosCount() throws Exception {
        assertTrue(mediaService.countUserVideos(2) == 3);
    }

    @Test
    public void test4DeletePhoto() throws Exception {
        mediaService.deletePhoto(2);
        assertFalse(mediaService
                .getAllPhotos()
                .stream()
                .anyMatch(item -> item.getPhotoID() == 2)
        );
    }

    @Test
    public void test5DeleteVideo() throws Exception {
        mediaService.deleteVideo(2);
        assertFalse(mediaService
                .getAllVideos()
                .stream()
                .anyMatch(item -> item.getVideoID() == 2)
        );
    }

    @Test
    public void testGetPhoto() throws Exception {
        Photo photo = mediaService.getPhotoById(1);
        assertNotNull(photo);
        assertEquals(photo.getFileName(), "testPhoto1");
        assertEquals(photo.getExtension(), "jpeg");
        assertTrue(photo.getSize() == 12345);
    }

    @Test
    public void testGetVideo() throws Exception {
        Video video = mediaService.getVideoById(1);
        assertNotNull(video);
        assertEquals(video.getFileName(), "testVideo1");
        assertEquals(video.getExtension(), "mp4");
        assertTrue(video.getSize() == 1234);
    }
}
