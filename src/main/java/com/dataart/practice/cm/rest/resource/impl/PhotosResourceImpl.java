package com.dataart.practice.cm.rest.resource.impl;

import com.dataart.practice.cm.data.model.Photo;
import com.dataart.practice.cm.rest.resource.BaseResource;
import com.dataart.practice.cm.rest.resource.PhotosResource;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by aovsyannikov on 9/1/2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PhotosResourceImpl extends BaseResource implements PhotosResource {

    @Cacheable("photos")
    public ResponseEntity<List<Resource>> getPhotos(
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "0")
                    int pageSize) {
        LinkedList<Resource> resourceList = new LinkedList<>();
        try {
            for (Photo photo : mediaService.getAllPhotos().stream().sorted().collect(Collectors.toList())) {
                resourceList.addLast(turnPhotoToResource(photo));
            }
            return performPagination(resourceList, page, pageSize);
        } catch (Exception e) {
            resourceList.add(new Resource<>(e.getMessage()));
        }
        return new ResponseEntity<>(resourceList, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Resource> getPhotosCount() {
        Resource resource = new Resource<>(-1);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = new Resource<>(mediaService.countAllPhotos(),
                    linkTo(PhotosResourceImpl.class).withRel("photos"));
        } catch (Exception e) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @Cacheable("photos")
    public ResponseEntity<Resource> getPhoto(@PathVariable(value = "photoID") long photoID) {
        Resource resource = new Resource<>("Internal error occurred: ");
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(cacheControl);
        HttpStatus status = HttpStatus.OK;
        try {
            resource = turnPhotoToResource(mediaService.getPhotoById(photoID));
        } catch (Exception e) {
            resource = new Resource<>(resource.getContent() + e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resource, headers, status);
    }

    @CacheEvict(cacheNames = {"photos", "userPhotos"}, allEntries = true)
    public ResponseEntity<Resource> deletePhotoById(
            @PathVariable(value = "photoID") long photoID, HttpSession session) {
        return deletePhoto(photoID, session);
    }

    @Cacheable(value = "photos", key = "#photoID")
    public void downloadPhotoById(@PathVariable("photoID") long photoID, HttpServletResponse resp) {
        downloadPhoto(photoID, resp);
    }
}
